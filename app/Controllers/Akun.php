<?php namespace App\Controllers;

use App\Models\UserAkunModel;
use App\Models\SessionAkunModel;
use App\Models\ProposalModel;
use App\Models\LupaPassModel;

class Akun extends BaseController
{
	protected $userAkun;
	protected $sessionLog;
	protected $mdlProposal;
	protected $mdlLupaPass;
	protected $email;
	protected $validation;
	protected $session;
	public function __construct()
	{
		$this->userAkun = new UserAkunModel();
		$this->sessionLog = new SessionAkunModel();
		$this->mdlProposal = new ProposalModel();
		$this->mdlLupaPass = new LupaPassModel();
		$this->email = \Config\Services::email();
		$this->validation = \Config\Services::validation();
		$this->session = \Config\Services::session();
	}

	public function index()
	{
		return view('daftar');
	}

//=================================================================FUNCTION BUAT AKUN
	public function buatAkun()
	{
		// ------ JIKA METHOD BUKAN POST -------
		$method = $this->request->getMethod();
		if ($method !== 'post') {
			return redirect()->to('/');
		}

		// === VALIDASI BUAT AKUN
		if (!$this->validate([
			"nm_lengkap"		=> [
				"rules" => "required|max_length[150]",
				"errors" => [
					"required"	=> "Nama lengkap harus diisi",
					"max_length"	=> "Maksimal 150 karakter"
				]
			],
			"email"				=> [
				"rules"	=> "required|valid_email|is_unique[user_akun.email]|max_length[150]",
				"errors" => [
					"required"	=> "Email harus diisi",
					"valid_email" => "Email tidak valid",
					"is_unique" => "Email sudah terdaftar",
					"max_length" => "Maksimal 150 karakter"
				]
			],
			"no_hp"				=> [
				"rules" => "required|decimal",
				"errors" => [
					"required" => "Nomor HP harus diisi",
					"decimal" => "Nomor HP tidak valid"
				]
			],
			"password"			=> [
				"rules" => "required|min_length[8]|max_length[255]",
				"errors" => [
					"required" => "Password harus diisi",
					"min_length" => "Password minimal 8 karakter",
					"max_length" => "Maksimal 255 karakter"
				]
			],
			"pass_confirm"		=> [
				"rules" => "required_with[password]|matches[password]",
				"errors" => [
					"required_with" => "Konfirmasi password harus diisi",
					"matches" => "Konfirmasi password tidak sesuai"
				]
			]
		])) {
			$error = $this->validation->getErrors();
			$gagal = [
				"success" => false,
				"error"   => $error
			];

			return $this->response->setJSON($gagal);
		}

		//------ AMBIL DATA POST -------
		$password = $this->request->getPost("password");
		$options = array("cost" => 11);
		$hash = password_hash($password, PASSWORD_BCRYPT, $options);
		$data = [
			"nm_lengkap" => $this->request->getPost("nm_lengkap"),
			"email" => $this->request->getPost("email"),
			"no_hp" => $this->request->getPost("no_hp"),
			"password" => $hash,
			"pass_confirm" => $this->request->getPost("pass_confirm")
		];
		$akunBaru = $this->userAkun->save($data);
		//------ JIKA GAGAL DISIMPAN -------
		if (!$akunBaru) {
			$gagal = [
				"success" => false,
				"error" => "Gagal disimpan"
			];
			return $this->response->setJSON($gagal);
		}

		//------ JIKA BERHASIL DISIMPAN -------
		$berhasil = [
			"success" => true,
			"message" => "Data berhasil disimpan"
		];
		return $this->response->setJSON($berhasil);
	}
//================================================================= END FUNCTION BUAT AKUN

//=================================================================FUNCTION LOGIN AKUN
	public function loginAkun()
	{
		if ($this->request->getMethod() !== "post") {
			return redirect()->to('/');
		}
		// COOKIE
		helper("cookie");

		//  ----------- VALIDASI DATA LOGIN
		$email = $this->request->getPost("email");
		$passwd = $this->request->getPost("passwd");
		$remember = $this->request->getPost("remember");
		if ($email === '' && $passwd === '') {
			$gagal = [
				"success" => false,
				"error" => [
					"email" => "Email harus diisi",
					"password" => "Password harus diisi"
				]
			];
			return $this->response->setJSON($gagal);
		} else if ($email === '') {
			$gagal = [
				"success" => false,
				"error" => [
					"email" => "Email harus diisi",
				]
			];
			return $this->response->setJSON($gagal);
		} else if (!filter_var($email, FILTER_VALIDATE_EMAIL) && $passwd === '') {
			$gagal = [
				"success" => false,
				"error" => [
					"email" => "Email tidak valid",
					"password" => "Password harus diisi",
				]
			];
			return $this->response->setJSON($gagal);
		} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$gagal = [
				"success" => false,
				"error" => [
					"email" => "Email tidak valid",
				]
			];
			return $this->response->setJSON($gagal);
		} else if ($passwd === '') {
			$gagal = [
				"success" => false,
				"error" => [
					"password" => "Password harus diisi",
				]
			];
			return $this->response->setJSON($gagal);
		}
		// ----------- END VALIDASI DATA LOGIN

		// ----------- CEK APAKAH EMAIL TERDAFTAR
		$cekEmail = $this->userAkun->cekEmail($email);
		//------ JIKA EMAIL TIDAK TERDAFTAR -------
		if (count($cekEmail->getResult()) < 1) {
			$gagal = [
				"success" => false,
				"message" => "Anda belum terdaftar"
			];
			return $this->response->setJSON($gagal);
		} else {
			//------ JIKA EMAIL TERDAFTAR AMBIL PASSWORD -------
			$data = $cekEmail->getResultArray();
			$password = $data[0]["password"];
			if (password_verify($passwd, $password)) {
				// ---------------- JIKA LOGIN BERHASIL
					// --- BUAT TOKEN LOGIN
				$length = 12;
				$token = bin2hex(random_bytes($length));
				$sessionData = [
					"id_akun" => $data[0]["id"],
					"token" => $token,
					"is_login" => true
				];
				$saveToken = $this->sessionLog->save($sessionData);
					// --- END BUAT TOKEN LOGIN
				$berhasil = [
					"success" => true,
					"message" => "Login berhasil"//,
					//"session_id"		=> $data[0]["id"],
					//"session_token"		=> $token,
				];
				// ---------- Buat Session / COOKIE di PHP
				if($remember) {
					set_cookie("is_login", true, 7200);
					set_cookie("token", $token, 7200);
					set_cookie("id_akun", $data[0]["id"], 7200);
				} else {
					$this->session->set($sessionData);
				}
				// $this->session->set($sessionData);
				return $this->response->setJSON($berhasil);
			} else {
				// ---------------- JIKA PASSWORD SALAH
				$gagal = [
					"success" => false,
					"error" => [
						"password" => "Password salah"
					]
				];
				return $this->response->setJSON($gagal);
			}
		}
		// ----------- END CEK APAKAH EMAIL TERDAFTAR
	}
//================================================================= END FUNCTION LOGIN AKUN

//================================================================= FUNCTION AMBIL DATA LOGIN
public function ambilLogin() {
	if ($this->request->getMethod() !== "post") {
		return redirect()->to('/');
	}

	$idAkun = $this->request->getPost("session_id");
	$ambilData = $this->userAkun->cekData($idAkun)->getResultArray();
	if (count($ambilData) < 1) {
		$gagal = [
			"success" => false,
			"message" => "Terjadi kesalahan"
		];
		return $this->response->setJSON($gagal);
	}
	$data = [
		"nm_lengkap" => $ambilData[0]["nm_lengkap"],
		"email" => $ambilData[0]["email"],
		"no_hp" => $ambilData[0]["no_hp"],
		"id" => $ambilData[0]["id"],
	];
	return $this->response->setJSON($data);
}
//================================================================= END FUNCTION AMBIL DATA LOGIN

//================================================================= FUNCTION HAPUS SESSION
public function hapusSession() {
	// COOKIE
	helper('cookie');
	if ($this->request->getMethod() !== "post") {
		return redirect()->to('/');
	}
	$id_akun = $this->request->getPost("id_akun");
	$delSession = $this->sessionLog->where("id_akun", $id_akun)->delete();
	$this->session->destroy();
	delete_cookie("is_login");
	delete_cookie("token");
	delete_cookie("id_akun");
}
//================================================================= END FUNCTION HAPUS SESSION

//================================================================= FUNCTION UPLOAD PROPOSAL
public function uploadProposal() {
	if ($this->request->getMethod() !== "post") {
		return redirect()->to('/');
	}

	// === VALIDASI INPUT PROPOSAL
	if (!$this->validate([
			"nm_lengkap"		=> [
				"rules" => "required|max_length[150]",
				"errors" => [
					"required"	=> "Nama lengkap harus diisi",
					"max_length"	=> "Maksimal 150 karakter"
				]
			],
			"email"		=> [
				"rules"	=> "required|valid_email|is_not_unique[user_akun.email]",
				"errors" => [
					"required" 			=> "Email harus diisi",
					"valid_email" 	=> "Email tidak valid",
					"is_not_unique" => "Email belum terdaftar"
				]
			],
			"no_hp"		=> [
				"rules"	=> "required|decimal|max_length[15]",
				"errors" => [
					"required" 		=> "Nomor HP harus diisi",
					"decimal"			=> "Isi dengan angka",
					"max_length"	=> "Maksimal 15 digit"
				]
			],
			"tempat_lahir"	=> [
				"rules"		=> "required|max_length[150]",
				"errors"	=> [
					"required"		=> "Tempat lahir harus diisi",
					"max_length"	=> "Maksimal 150 karakter"
				]
			],
			"tgl_lahir"	=> [
				"rules"		=> "required|valid_date[Y-m-d]",
				"errors"	=> [
					"required"		=> "Tanggal lahir harus diisi",
					"valid_date"	=> "Tanggal tidak valid [mm/dd/yyyy]"
				]
			],
			"kota_domisili"	=> [
				"rules"		=> "required|max_length[150]",
				"errors"	=> [
					"required"		=> "Kota domisili harus diisi",
					"max_length"	=> "Maksimal 150 karakter"
				]
			],
			"pekerjaan"		=> [
				"rules"		=> "required|max_length[125]",
				"errors"	=> [
					"required"		=> "Pekerjaan wajib diisi",
					"max_length"	=> "Maksimal 125 karakter"
				]
			],
			"jenis_kelamin"	=> [
				"rules"		=> "required",
				"errors"	=> [
					"required"	=> "Jenis kelamin wajib diisi"
				]
			],
			"role"	=> [
				"rules"		=> "required",
				"errors"	=> [
					"required"	=> "Role wajib diisi"
				]
			],
			"url_proposal"	=> [
				"rules"		=> "uploaded[url_proposal]|max_size[url_proposal,10048]|mime_in[url_proposal,application/pdf]|ext_in[url_proposal,pdf]",
				"errors"	=> [
					"uploaded"	=> "Wajib upload proposal",
					"max_size"	=> "Ukuran file maksimal 10MB",
					"mime_in"		=> "Tipe data yang diizinkan PDF",
					"ext_in"		=> "Tipe data yang diizinkan PDF"
				]
			],
			"bidang_bisnis"	=> [
				"rules"		=> "required|max_length[150]",
				"errors"	=> [
					"required"		=> "Bidang bisnis harus diisi",
					"max_length"	=> "Maksimal 150 karakter"
				]
			],
			"metode_bayar"	=> [
				"rules"		=> "required",
				"errors"	=> [
					"required"	=> "Pilih metode bayar"
				]
			],
			"url_bukti_bayar"	=> [
				"rules"		=> "uploaded[url_bukti_bayar]|max_size[url_bukti_bayar,5048]|mime_in[url_bukti_bayar,image/png,image/jpg,image/jpeg]|ext_in[url_bukti_bayar,png,jpg,jpeg]",
				"errors"	=> [
					"uploaded"	=> "Wajib upload bukti bayar",
					"max_size"	=> "Ukuran file maksimal 5MB",
					"mime_in"		=> "Tipe file yang diizinkan png/jpg/jpeg",
					"ext_in"		=> "Tipe file yang diizinkan png/jpg/jpeg"
				]
			]
		])) {
			$error = $this->validation->getErrors();
			$gagal = [
				"success" => "false",
				"error" 	=> $error
			];

			return $this->response->setJSON($gagal);
	}
	// ambil file pdf
	$proposal = $this->request->getFile("url_proposal");
	$proposal->move('assets/proposal');
	$nmProposal = $proposal->getName();

	// ambil file gambar
	$bBayar = $this->request->getFile("url_bukti_bayar");
	$nmbBayar = $bBayar->getRandomName();
	$bBayar->move('assets/bukti_bayar', $nmbBayar);

	//ambil data-data
	$id_user = $this->request->getPost("id_user");
	$nm_lengkap = $this->request->getPost("nm_lengkap");
	$email = $this->request->getPost("email");
	$bidang_bisnis	=	$this->request->getPost("bidang_bisnis");
	$jenis_kelamin	=	$this->request->getPost("jenis_kelamin");
	$kota_domisili	= $this->request->getPost("kota_domisili");
	$metode_bayar		=	$this->request->getPost("metode_bayar");
	$no_hp		=	$this->request->getPost("no_hp");
	$pekerjaan		=	$this->request->getPost("pekerjaan");
	$role		=	$this->request->getPost("role");
	$tempat_lahir		=	$this->request->getPost("tempat_lahir");
	$tgl_lahir		=	$this->request->getPost("tgl_lahir");


	// simpan
	$simpan = $this->mdlProposal->save([
		"id_user"					=> $id_user,
		"nm_lengkap"			=> $nm_lengkap,
		"email"						=> $email,
		"bidang_bisnis"		=> $bidang_bisnis,
		"jenis_kelamin"		=> $jenis_kelamin,
		"kota_domisili"		=> $kota_domisili,
		"metode_bayar"		=> $metode_bayar,
		"no_hp"						=> $no_hp,
		"pekerjaan"				=> $pekerjaan,
		"role"						=> $role,
		"tempat_lahir"		=> $tempat_lahir,
		"tgl_lahir"				=> $tgl_lahir,
		"url_proposal"		=> $nmProposal,
		"url_bukti_bayar"	=> $nmbBayar
	]);

	if (!$simpan) {
		$gagal = [
			"success"	=> false,
			"message"	=> "Ada kesalahan"
		];
		return $this->response->setJSON($gagal);
	}
	$berhasil = [
		"success"	=> true,
		"message"	=> "Data berhasil disimpan"
	];
	//sendemail konfirmasi berhasil
	$this->sendEmailKonfirm($email);
	return $this->response->setJSON($berhasil);
}
//================================================================= END FUNCTION UPLOAD PROPOSAL

// ========================== FUNCTION LUPA PASSWORD

public function lupaPass() {
	if ($this->request->getMethod() !== "post") {
		return redirect()->to('/');
	}

	// === VALIDASI INPUT EMAIL FORM LUPA PASSWORD
	if (!$this->validate([
			"email"		=> [
				"rules" => "required|valid_email|is_not_unique[user_akun.email]",
				"errors" => [
					"required"	=> "Email harus diisi",
					"valid_email"	=> "Email tidak valid",
					"is_not_unique" => "Email belum terdaftar"
				]
			]
		])) {
			$error = $this->validation->getErrors();
			$gagal = [
				"success" => "false",
				"error" 	=> $error
			];
			return $this->response->setJSON($gagal);
	}
	// -----SIMPAN EMAIL DAN TOKEN
	$email = $this->request->getPost("email");
	//----- buat token random
	$token = mt_rand(100000, 999999);
	// ----- SIMPAN KE DB
	$smpToken = $this->mdlLupaPass->save([
		"email"	=> $email,
		"temp_token"	=>	$token
	]);
		//--- KIRIM EMAIL
		$krmToken = $this->sendEmail($email, $token);
		if (!$krmToken) {
			$gagal = [
				"success" => false,
				// "message" => "Kode reset gagal dikirim"
				"message" => $this->email->printDebugger(['headers'])
			];
			return $this->response->setJSON($gagal);
		} else {
			$berhasil = [
				"success" => true,
				"message" => "Kode reset berhasil dikirim <br/><b>Cek Email Anda</b>"
			];
			return $this->response->setJSON($berhasil);
		}
		//--- END KIRIM EMAIL
	// ---- END SIMPAN EMAIL DAN TOKEN
}
// ========================== END FUNCTION LUPA PASSWORD

// =========================== FUNCTION UBAH PASSWORD
public function ubahPass() {
	if ($this->request->getMethod() !== "post") {
		return redirect()->to('/');
	}
	// === VALIDASI INPUT ALL FORM LUPA PASSWORD
		if (!$this->validate([
				"email"		=> [
					"rules" => "required|valid_email|is_not_unique[user_akun.email]",
					"errors" => [
						"required"	=> "Email harus diisi",
						"valid_email"	=> "Email tidak valid",
						"is_not_unique" => "Email belum terdaftar"
					]
				],
				"kodeReset" => [
					"rules" => "required|max_length[6]",
					"errors"	=> [
						"required"	=> "Kode harus diisi",
						"max_length"	=> "Maksimal 6 karakter"
					]
				],
				"passwd" => [
					"rules"	=> "required|min_length[8]|max_length[255]",
					"errors" => [
						"required"	=> "Password baru harus diisi",
						"min_length" => "Minimal 8 karakter",
						"max_length"	=> "Maksimal 255 karakter"
					]
				],
				"konfPasswd" => [
					"rules"	=> "required_with[passwd]|matches[passwd]",
					"errors" => [
						"required_with"	=> "Konfirmasi password harus diisi",
						"matches"	=> "Konfirmasi password tidak sesuai"
					]
				]
			])) {
				$error = $this->validation->getErrors();
				$gagal = [
					"success" => "false",
					"error" 	=> $error
				];
				return $this->response->setJSON($gagal);
		}
		// --- SETELAH VALIDASI BERHASIL
		$email = $this->request->getPost("email");
		$token = $this->request->getPost("kodeReset");
		$passwd = $this->request->getPost("passwd");
		$options = array("cost" => 11);
		$hash = password_hash($passwd, PASSWORD_BCRYPT, $options);
		$cekTokenDt = $this->mdlLupaPass->cekToken($token, $email);
		$jml = count($cekTokenDt->getResultArray());
		if ($jml > 0) {
			// -- UBAH PASSWORD
			$ubahPass = $this->userAkun->set('password', $hash)->where('email', $email)->update();
			$delToken = $this->mdlLupaPass->where('temp_token', $token)->delete();
			if (!$ubahPass) {
				$gagal = [
					"success" => false,
					"message"	=> "Password gagal diubah"
				];
				return $this->response->setJSON($gagal);
			} else {
				$berhasil = [
					"success" => true,
					"message"	=> "Password berhasil diubah"
				];
				return $this->response->setJSON($berhasil);
			}
		}
		$gagal = [
			"success" => false,
			"message"	=> "Kode reset salah"
		];
		return $this->response->setJSON($gagal);
}
// ============================ END FUNCTION UBAH PASSWORD
// ========================== FUNCTION SEND EMAIL TOKEN
private function sendEmail($to, $token) {
	$this->email->setFrom('engineering@merintisindonesia.com', 'MERINTIS INDONESIA');
	$this->email->setTo($to);
	$this->email->setSubject('RESET PASSWORD MERINTIS INDONESIA');
	$this->email->setMessage('<h1>Reset Password Akun Merintis Indonesia</h1>
	<p>Berikut adalah token reset password: '.$token.'</p>
	<p>Jangan tunjukkan kode kepada siapapun</p>');
	if (! $this->email->send()) {
		$this->email->send(false);
		return false;
	} else {
		return true;
	}
}
// ========================== END FUNCTION SEND EMAIL TOKEN
// ========================== FUNCTION SEND EMAIL KONFIRMASI PROPOSAL
private function sendEmailKonfirm($to) {
	$this->email->setFrom('engineering@merintisindonesia.com', 'MERINTIS INDONESIA');
	$this->email->setTo($to);
	$this->email->setSubject('[INFO] MERINTIS INDONESIA SUMMIT 2021');
	$this->email->setMessage('<h1>Salam Pengusaha Muda!</h1>
	<p>Halo, Sedulur!</p>
	<p>Kami infokan bahwa Poposal Bisnis Sedulur sudah masuk dalam data dan diterima oleh Tim Merintis Indonesia.</p>
	<p>Untuk pengumuman TOP 50 Finalis akan diumumkan secara resmi melalui website <a href="https://www.merintisindonesia.com">www.merintisindonesia.com</a> dan instagram <a href="https://www.instagram.com/merintis.indonesia/">@merintis.indonesia</a> pada tanggal 7 Februari 2021.</p>
	<p>Untuk informasi lebih lanjut silahkan hubungi Admin Merintis Indonesia(<a href="https://wa.me/+6285785036770">+62 857 8503 6770</a>).</p>
	<p>#MerintisIndonesiaBersamaPengusahaMuda</p>');
	if (! $this->email->send()) {
		$this->email->send(false);
		return false;
	} else {
		return true;
	}
}
// ========================== END FUNCTION SEND EMAIL KONFIRMASI PROPOSAL

	//--------------------------------------------------------------------

}
