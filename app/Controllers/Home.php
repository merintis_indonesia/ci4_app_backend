<?php namespace App\Controllers;

use App\Models\PengumumanModel;
use App\Models\ProgramModel;

class Home extends BaseController
{
	protected $session;
	protected $mdlPengumuman;
	protected $mdlProgram;
	protected $validation;
	public function __construct()
	{
		$this->session = \Config\Services::session();
		$this->mdlPengumuman = new PengumumanModel();
		$this->mdlProgram = new ProgramModel();
		$this->validation = \Config\Services::validation();
	}

	public function index()
	{
		return view('home');
	}

	// public function mis()
	// {
	// 	if ($this->session->has("is_login")) {
	// 		return view('mis');
	// 	} else {
	// 		return redirect()->route('/');
	// 	}
	// 	// dd($this->session->get("is_login"));
	// }

	// public function pengumuman()
	// {
	// 	if (!$this->session->has("is_login")) {
	// 		return redirect()->route('/');
	// 	}
	// 	$dtPengumuman = $this->mdlPengumuman->tampilData()->getResultArray();
	// 	//var_dump($dtPengumuman);
	// 	if(sizeof($dtPengumuman) > 0) {
	// 		return $this->response->setJSON($dtPengumuman);
	// 	} else {
	// 		$gagal = [
	// 			"pesan" => "Gagal mengambil data"
	// 		];
	// 		return $this->response->setJSON($gagal);
	// 	}
	// }

	// public function cariDataFinalis() {
	// 	if (!$this->session->has("is_login")) {
	// 		return redirect()->route('/');
	// 	}
	// 	$nama_tim = $this->request->getPost("nama_tim");
	// 	$dtPengumuman = $this->mdlPengumuman->cariData($nama_tim)->getResultArray();
	// 	//var_dump($dtPengumuman);
	// 	if(sizeof($dtPengumuman) > 0) {
	// 		return $this->response->setJSON($dtPengumuman);
	// 	} else {
	// 		$gagal = [
	// 			"pesan" => "Gagal mengambil data"
	// 		];
	// 		return $this->response->setJSON($gagal);
	// 	}
	// }

	// public function uploadLink() {
	// 	if (!$this->session->has("is_login")) {
	// 		return redirect()->route('/');
	// 	}
	// 	if (!$this->validate([
	// 		"videopitch" => [
	// 			"rules" => "required",
	// 			"errors" => [
	// 				"required" => "*Link Video Youtube Wajib Diisi"
	// 			]
	// 		]
	// 	])) {
	// 		$error = $this->validation->getErrors();
	// 		$gagal = [
	// 			"success" => "false",
	// 			"error" 	=> $error
	// 		];
	// 		return $this->response->setJSON($gagal);
	// 	}

	// 	//simpan data
	// 	$idAkun = $this->request->getPost('id_user');
	// 	$linkvideo = $this->request->getPost('videopitch');
	// 	$smpnLink = $this->mdlPengumuman->simpanLink($idAkun, $linkvideo);
	// 	if ($smpnLink) {
	// 		$berhasil = [
	// 			"success" => "true",
	// 			"message" 	=> "Berhasil Disimpan"
	// 		];
	// 		return $this->response->setJSON($berhasil);
	// 	} else {
	// 		$gagal = [
	// 			"success" => "false",
	// 			"error" 	=> "Koneksi Bermasalah!"
	// 		];
	// 		return $this->response->setJSON($gagal);
	// 	}

	// }

	// public function cekFinalisMIS() {
	// 	if (!$this->session->has("is_login")) {
	// 		return redirect()->route('/');
	// 	}
	// 	$idAkun = $this->request->getPost('id_akun');
	// 	$cekData = $this->mdlPengumuman->cekFinalis($idAkun)->getResultArray();
	// 	if (sizeof($cekData) > 0) {
	// 		$berhasil = [
	// 			"success" => "true"
	// 		];
	// 		return $this->response->setJSON($berhasil);
	// 	} else {
	// 		$gagal = [
	// 			"success" => "false",
	// 			"error" => "Maaf, Anda belum lolos"
	// 		];
	// 		return $this->response->setJSON($gagal);
	// 	}
	// }

	public function program() {
		// COOKIE
		helper('cookie');
		$isLogin = ($this->session->has("is_login") ? $this->session->get("is_login") : get_cookie("is_login"));
		if (!$isLogin) {
			return redirect()->route('/');	
		}
		return view('program');
	}

// Proses Ambil Data Program
	// PAST
	public function pastprogram() {
		// COOKIE
		helper('cookie');
		$isLogin = ($this->session->has("is_login") ? $this->session->get("is_login") : get_cookie("is_login"));
		if (!$isLogin) {
			return redirect()->route('/');	
		}
		$pastProg = $this->mdlProgram->getPastProgram()->getResult();
		return $this->response->setJSON($pastProg);
	}

	// ONGOING
	public function ongoingprogram() {
		// COOKIE
		helper('cookie');
		$isLogin = ($this->session->has("is_login") ? $this->session->get("is_login") : get_cookie("is_login"));
		if (!$isLogin) {
			return redirect()->route('/');	
		}
		$ongoProg = $this->mdlProgram->getOngoingProgram()->getResult();
		return $this->response->setJSON($ongoProg);
	}

	// ONGOING DETAIL
	public function ongoingdetail($id) {
		// COOKIE
		helper('cookie');
		$isLogin = ($this->session->has("is_login") ? $this->session->get("is_login") : get_cookie("is_login"));
		if (!$isLogin) {
			return redirect()->route('/');	
		}
		$dtDetailOngo = $this->mdlProgram->getByIdOngoing($id)->getResultArray();
		$data = [
			"detailOngo" => $dtDetailOngo
		];
		return view('detailongoing', $data);
	}

	// UPCOMING


	public function signin() {
		// COOKIE
		helper('cookie');
		$isLogin = ($this->session->has("is_login") ? $this->session->get("is_login") : get_cookie("is_login"));
		if ($isLogin) {
			return redirect()->route('/');	
		}
		return view('signin');
	}

	public function signup() {
		// COOKIE
		helper('cookie');
		$isLogin = ($this->session->has("is_login") ? $this->session->get("is_login") : get_cookie("is_login"));
		if ($isLogin) {
			return redirect()->route('/');	
		}
		return view('signup');
	}

	public function lupapass() {
		return view('lupapass');
	}

	//--------------------------------------------------------------------

}
