<?php

namespace App\Controllers;

use App\Models\AdminModel;
use App\Models\ProposalModel;
use App\Models\PengumumanModel;
use App\Models\ProgramModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

date_default_timezone_set("Asia/Jakarta");

class Miadmin extends BaseController
{
	protected $session;
	protected $mdlAdmin;
	protected $mdlProposal;
	protected $mdlPengumuman;
	protected $mdlProgram;
	protected $validation;
	public function __construct()
	{
		$this->session = \Config\Services::session();
		$this->mdlAdmin = new AdminModel();
		$this->mdlProposal = new ProposalModel();
		$this->mdlPengumuman = new PengumumanModel();
		$this->mdlProgram = new ProgramModel();
		$this->validation = \Config\Services::validation();
	}

	public function index()
	{
		if ($this->session->has("is_admin")) {
			return redirect()->route('miadmin/homeadmin');
		}
		return view('loginadmin');
	}

	public function cekadmin()
	{
		if ($this->request->getMethod() !== "post") {
			return redirect()->route('/');
		}
		if (!$this->validate([
			"uname" => [
				"rules" => "required",
				"errors" => [
					"required" => "Username wajib diisi"
				]
			],
			"password" => [
				"rules" => "required",
				"errors" => [
					"required" => "Password wajib diisi"
				]
			]
		])) {
			$error = $this->validation->getErrors();
			$gagal = [
				"success" => "false",
				"error" 	=> $error
			];
			return $this->response->setJSON($gagal);
		}
		$uname = $this->request->getPost('uname');
		$password = $this->request->getPost('password');
		$dataAdmin = $this->mdlAdmin->cekAdmin()->getResultArray();
		// $options = array("cost" => 11);
		// $hash = password_hash($password, PASSWORD_BCRYPT, $options);
		// echo $hash;

		if ($uname === $dataAdmin[0]['username']) {
			// username benar
			// cek password
			if (password_verify($password, $dataAdmin[0]['password'])) {
				$data = [
					"success" => "true",
					"message" => "Berhasil login"
				];
				$this->session->set("is_admin", true);
				return $this->response->setJSON($data);
			} else {
				$data = [
					"success" => "false",
					"error" => [
						"password" => "Password salah"
					]
				];
				return $this->response->setJSON($data);
			}
		} else {
			$data = [
				"success" => "false",
				"error" => [
					"uname" => "Username salah"
				]
			];
			return $this->response->setJSON($data);
		}
	}

	//---- HALAMAN DASHBOARD ADMIN
	public function homeadmin()
	{
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$dtProposal = $this->mdlProposal->dataProposal()->getResultArray();
		$angSubmit = 0;
		for ($i = 0; $i < count($dtProposal); $i++) {
			if ($dtProposal[$i]['url_proposal']) {
				$angSubmit++;
			}
		}
		$dtFinalis = $this->mdlPengumuman->tampilAll()->getResultArray();
		$angSubmitVid = 0;
		for ($i = 0; $i < count($dtFinalis); $i++) {
			if ($dtFinalis[$i]['link_yt']) {
				$angSubmitVid++;
			}
		}
		$jmlProgram = $this->mdlProgram->dataProgram()->getResultArray();
		$jmlProgram = count($jmlProgram);
		$data = [
			"dataProposal" => [
				"login" => count($dtProposal),
				"submit" => $angSubmit
			],
			"dataFinalis" => [
				"submit" => $angSubmitVid
			],
			"dataProgram" => [
				"jumlah" => $jmlProgram
			]
		];
		return view('adminhome', $data);
	}

	public function datamis()
	{
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$dtProposal = $this->mdlProposal->dataProposal()->getResultArray();
		$data = [
			"dtProposal" => $dtProposal
		];
		return view('datamis', $data);
	}

	public function datafinalis()
	{
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$dataFinalis = $this->mdlPengumuman->tampilAll()->getResultArray();
		$data = [
			'dataFinalis' => $dataFinalis
		];
		return view('datafinalis', $data);
	}

	public function exportexcel()
	{
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$dtProposal = $this->mdlProposal->dataProposal()->getResultArray();
		// dd($dtProposal);

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getDefaultStyle()->getFont()->setName('Calibri');
		$spreadsheet->getDefaultStyle()->getFont()->setSize(12);

		//Mengatur Width Columns
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(35);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(43);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(24);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(14);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(17);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(17);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(14);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(32);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(22);
		$spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(32);
		$spreadsheet->getActiveSheet()->getStyle('A5:N' . (count($dtProposal) + 4))->getAlignment()->setWrapText(true);
		$arrayBorders = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];
		$spreadsheet->getActiveSheet()->getStyle('A4:N' . (count($dtProposal) + 4))->applyFromArray($arrayBorders);

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->mergeCells('A2:N2');
		$sheet->setCellValue('A2', 'PESERTA MERINTIS INDONESIA SUMMIT 2021');
		$sheet->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('A2')->getFont()->setBold(true);
		$sheet->getStyle('A4:N4')->getFont()->setBold(true);
		$sheet->setCellValue('A4', 'No');
		$sheet->setCellValue('B4', 'Nama');
		$sheet->setCellValue('C4', 'Email');
		$sheet->setCellValue('D4', 'Nomor HP');
		$sheet->setCellValue('E4', 'Tempat Lahir');
		$sheet->setCellValue('F4', 'Tanggal Lahir');
		$sheet->setCellValue('G4', 'Kota Domisili');
		$sheet->setCellValue('H4', 'Pekerjaan');
		$sheet->setCellValue('I4', 'Jenis Kelamin');
		$sheet->setCellValue('J4', 'Role');
		$sheet->setCellValue('K4', 'Proposal');
		$sheet->setCellValue('L4', 'Bidang Bisnis');
		$sheet->setCellValue('M4', 'Metode Bayar');
		$sheet->setCellValue('N4', 'Bukti Bayar');
		$sheet->getStyle('B5:N' . (count($dtProposal) + 4))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		//Looping Foreach dtProposal
		$baris = 5;
		for ($i = 0; $i < count($dtProposal); $i++) {
			$sheet->setCellValue('A' . $baris, $i + 1);
			$sheet->getStyle('A' . $baris)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle('A' . $baris)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			$sheet->setCellValue('B' . $baris, $dtProposal[$i]['nm_lengkap']);
			$sheet->setCellValue('C' . $baris, $dtProposal[$i]['email']);
			$sheet->getStyle('C' . $baris)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
			$sheet->getStyle('C' . $baris)->getFont()->setUnderline(true);
			$spreadsheet->getActiveSheet()->getCell('C' . $baris)->getHyperlink()->setUrl('mailto:' . $dtProposal[$i]['email']);
			$sheet->setCellValue('D' . $baris, $dtProposal[$i]['no_hp']);
			$spreadsheet->getActiveSheet()->getStyle('D' . $baris)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
			$sheet->setCellValue('E' . $baris, $dtProposal[$i]['tempat_lahir']);
			$sheet->setCellValue('F' . $baris, $dtProposal[$i]['tgl_lahir']);
			$spreadsheet->getActiveSheet()->getStyle('F' . $baris)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			$sheet->setCellValue('G' . $baris, $dtProposal[$i]['kota_domisili']);
			$sheet->setCellValue('H' . $baris, $dtProposal[$i]['pekerjaan']);
			$sheet->setCellValue('I' . $baris, $dtProposal[$i]['jenis_kelamin']);
			$sheet->setCellValue('J' . $baris, $dtProposal[$i]['role']);
			if ($dtProposal[$i]['url_proposal']) {
				$sheet->setCellValue('K' . $baris, base_url('/assets/proposal/' . $dtProposal[$i]['url_proposal']));
				$spreadsheet->getActiveSheet()->getCell('K' . $baris)->getHyperlink()->setUrl(base_url('/assets/proposal/' . $dtProposal[$i]['url_proposal']));
			}
			$sheet->getStyle('K' . $baris)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
			$sheet->getStyle('K' . $baris)->getFont()->setUnderline(true);
			$sheet->setCellValue('L' . $baris, $dtProposal[$i]['bidang_bisnis']);
			$sheet->setCellValue('M' . $baris, $dtProposal[$i]['metode_bayar']);
			if ($dtProposal[$i]['url_bukti_bayar']) {
				$sheet->setCellValue('N' . $baris, base_url('/assets/bukti_bayar/' . $dtProposal[$i]['url_bukti_bayar']));
				$spreadsheet->getActiveSheet()->getCell('N' . $baris)->getHyperlink()->setUrl(base_url('assets/bukti_bayar/' . $dtProposal[$i]['url_bukti_bayar']));
			}
			$sheet->getStyle('N' . $baris)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
			$sheet->getStyle('N' . $baris)->getFont()->setUnderline(true);
			$baris++;
		}

		// Jumlah Submit dan Login + Last Updated
		$sheet->setCellValue('B' . (count($dtProposal) + 7), 'Last Updated: ' . date("d/m/Y"));
		$sheet->setCellValue('C' . (count($dtProposal) + 7), 'Peserta Login: ' . count($dtProposal));
		$jmlSubmit = 0;
		for ($i = 0; $i < count($dtProposal); $i++) {
			if ($dtProposal[$i]['url_proposal']) {
				$jmlSubmit++;
			}
		}
		$sheet->setCellValue('D' . (count($dtProposal) + 7), 'Submit Proposal: ' . $jmlSubmit);

		$filename = 'Rekap-Peserta-Merintis_Indonesia';
		ob_end_flush();
		ob_end_clean();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
		exit;
	}

	public function exportxlsfinal()
	{
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$dtFinalis = $this->mdlPengumuman->tampilAll()->getResultArray();

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getDefaultStyle()->getFont()->setName('Calibri');
		$spreadsheet->getDefaultStyle()->getFont()->setSize(12);

		//Mengatur Width Columns
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(35);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(24);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(32);
		$spreadsheet->getActiveSheet()->getStyle('A5:E' . (count($dtFinalis) + 5))->getAlignment()->setWrapText(true);
		$arrayBorders = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];
		$spreadsheet->getActiveSheet()->getStyle('A5:E' . (count($dtFinalis) + 5))->applyFromArray($arrayBorders);

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->mergeCells('A2:E2');
		$sheet->setCellValue('A2', 'FINALIS TOP 50');
		$sheet->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('A2')->getFont()->setBold(true);
		$sheet->mergeCells('A3:E3');
		$sheet->setCellValue('A3', 'MERINTIS INDONESIA SUMMIT 2021');
		$sheet->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('A3')->getFont()->setBold(true);
		$sheet->getStyle('A5:E5')->getFont()->setBold(true);
		$sheet->setCellValue('A5', 'No');
		$sheet->setCellValue('B5', 'ID Finalis');
		$sheet->setCellValue('C5', 'Nama Tim');
		$sheet->setCellValue('D5', 'Nama Bisnis');
		$sheet->setCellValue('E5', 'Link Video');
		$sheet->getStyle('B5:E' . (count($dtFinalis) + 4))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		//Looping Foreach dtProposal
		$baris = 6;
		for ($i = 0; $i < count($dtFinalis); $i++) {
			$sheet->setCellValue('A' . $baris, $i + 1);
			$sheet->getStyle('A' . $baris)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle('A' . $baris)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			$sheet->setCellValue('B' . $baris, $dtFinalis[$i]['id_finalis']);
			$sheet->getStyle('B6:B' . (count($dtFinalis) + 5))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->setCellValue('C' . $baris, $dtFinalis[$i]['nm_lengkap']);
			$sheet->setCellValue('D' . $baris, $dtFinalis[$i]['nama_bisnis']);
			$sheet->setCellValue('E' . $baris, $dtFinalis[$i]['link_yt']);
			if ($dtFinalis[$i]['link_yt']) {
				$spreadsheet->getActiveSheet()->getCell('E' . $baris)->getHyperlink()->setUrl($dtFinalis[$i]['link_yt']);
			}
			$sheet->getStyle('E' . $baris)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
			$sheet->getStyle('E' . $baris)->getFont()->setUnderline(true);
			$baris++;
		}
		// Jumlah Submit dan Login + Last Updated
		$sheet->setCellValue('C' . (count($dtFinalis) + 7), 'Last Updated: ' . date("d/m/Y"));
		$jmlSubmit = 0;
		for ($i = 0; $i < count($dtFinalis); $i++) {
			if ($dtFinalis[$i]['link_yt']) {
				$jmlSubmit++;
			}
		}
		$sheet->setCellValue('E' . (count($dtFinalis) + 7), 'Submit Video: ' . $jmlSubmit);

		$filename = 'Rekap-Finalis-Top50-Merintis_Indonesia';
		ob_end_flush();
		ob_end_clean();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
		exit;
	}

	public function logoutadmin()
	{
		//destroy session
		$this->session->destroy();
		return redirect()->route('miadmin/loginadmin');
	}

	public function dataprogram()
	{
		// Tampilkan semua program
		// Kasih session
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$dtProgram = $this->mdlProgram->dataProgram()->getResultArray();
		$data = [
			"dtProgram" => $dtProgram
		];
		return view('dataprogram', $data);
	}

	public function formprogram()
	{	
		// Kasih Session
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}

		return view('formprogram');
	}

	// MENGINPUTKAN PROGRAM BARU
	public function inputprogram()
	{
		// Kasih Session
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}

		if (!$this->validate([
			"nama_program" => [
				"rules" => "required",
				"errors" => [
					"required" => "Nama program wajib diisi"
				]
			],
			"nama_kegiatan" => [
				"rules" => "required",
				"errors" => [
					"required" => "Nama kegiatan wajib diisi"
				]
			],
			"link_pamflet" => [
				"rules" => "uploaded[link_pamflet]|mime_in[link_pamflet,image/png,image/jpg,image/jpeg]|max_size[link_pamflet,1524]",
				"errors" => [
					"uploaded" => "Unggah gambar pamflet",
					"mime_in" => "Tipe gambar yang diizinkan png/jpg/jpeg",
					"max_size" => "Ukuran gambar maksimal 1.5 MB"
				]
			],
			"desc_program" => [
				"rules" => "required",
				"errors" => [
					"required" => "Deskripsi program wajib diisi",
				]
			],
			"link_daftar" => [
				"rules" => "required|valid_url",
				"errors" => [
					"required" => "Link pendaftaran wajib diisi",
					"valid_url" => "URL tidak valid"
				]
			],
			"sasaran_program" => [
				"rules" => "required",
				"errors" => [
					"required" => "Sasaran program wajib diisi"
				]
			],
			"tgl_mulai" => [
				"rules" => "required",
				"errors" => [
					"required" => "Tanggal mulai wajib diisi"
				]
			],
			"jam_mulai" => [
				"rules" => "required|regex_match[/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/]",
				"errors" => [
					"required" => "Jam mulai wajib diisi",
					"regex_match" => "Format jam tidak valid"
				]
			],
			"jam_selesai" => [
				"rules" => "required|regex_match[/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/]",
				"errors" => [
					"required" => "Jam selesai wajib diisi",
					"regex_match" => "Format jam tidak valid"
				]
			],
			"biaya" => [
				"rules" => "required",
				"errors" => [
					"required" => "Wajib pilih salah satu"
				]
			],
			"status" => [
				"rules" => "required",
				"errors" => [
					"required" => "Status wajib diisi"
				]
			]
		])) {
			$error = $this->validation->getErrors();
			$gagal = [
				"success" => "false",
				"error" 	=> $error
			];
			return $this->response->setJSON($gagal);
		}

		// Mengolah dan Memindah File
		$nmFile = $this->request->getFile('link_pamflet');
		$nwName = $nmFile->getRandomName();
		$simpanPamflet = $nmFile->move('assets/img/program', $nwName);
		if ($simpanPamflet) {
			// Jika Lolos Validasi
			$data = [
				'nama_program' => $this->request->getPost('nama_program'),
				'nama_kegiatan' => $this->request->getPost('nama_kegiatan'),
				'link_pamflet' => $nwName,
				'link_daftar' => $this->request->getPost('link_daftar'),
				'desc_program' => $this->request->getPost('desc_program'),
				'sasaran_program' => $this->request->getPost('sasaran_program'),
				'tgl_mulai' => $this->request->getPost('tgl_mulai'),
				'tgl_selesai' => $this->request->getPost('tgl_selesai'),
				'jam_mulai' => $this->request->getPost('jam_mulai'),
				'jam_selesai' => $this->request->getPost('jam_selesai'),
				'biaya' => $this->request->getPost('biaya'),
				'harga_normal' => $this->request->getPost('harga_normal'),
				'harga_promo' => $this->request->getPost('harga_promo'),
				'link_jadwal' => $this->request->getPost('link_jadwal'),
				'link_dokumentasi' => $this->request->getPost('link_dokumentasi'),
				'status' => $this->request->getPost('status')
			];

			// Simpan data
			$simpanData = $this->mdlProgram->insert($data);
			if ($simpanData) {
				$berhasil = [
					"success" => "true",
					"message" => "Data berhasil disimpan"
				];
				return $this->response->setJSON($berhasil);
			} else {
				// Gagal
				$gagal = [
					"success" => "false",
					"message" => "Data gagal disimpan"
				];
				return $this->response->setJSON($gagal);
			}
		} else {
			// Gagal Simpan Pamflet
			$gagal = [
				"success" => "false",
				"message" => "Data gagal disimpan"
			];
			return $this->response->setJSON($gagal);
		}
	}

// ----- SHOW PROGRAM
	// Tampilkan Past-Program
	// Tampilkan Ongoing-Program
	// Tampilkan Detail-Ongoing-Program

// ------ CRUD PROGRAM
	// Update program
	public function ubahprogram($id)
	{
		// Kasih Session
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$dtProgram = $this->mdlProgram->getByIdProgram($id)->getResultArray();
		$data = [
			"dtProgram" => $dtProgram
		];
		return view('formprogram',$data);
	}

	public function prsubahprogram() 
	{
		// proses ubah program
		// Kasih Session
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}

		if (!$this->validate([
			"nama_program" => [
				"rules" => "required",
				"errors" => [
					"required" => "Nama program wajib diisi"
				]
			],
			"nama_kegiatan" => [
				"rules" => "required",
				"errors" => [
					"required" => "Nama kegiatan wajib diisi"
				]
			],
			"link_pamflet" => [
				"rules" => "mime_in[link_pamflet,image/png,image/jpg,image/jpeg]|max_size[link_pamflet,1524]",
				"errors" => [
					"mime_in" => "Tipe gambar yang diizinkan png/jpg/jpeg",
					"max_size" => "Ukuran gambar maksimal 1.5 MB"
				]
			],
			"desc_program" => [
				"rules" => "required",
				"errors" => [
					"required" => "Deskripsi program wajib diisi",
				]
			],
			"link_daftar" => [
				"rules" => "required|valid_url",
				"errors" => [
					"required" => "Link pendaftaran wajib diisi",
					"valid_url" => "URL tidak valid"
				]
			],
			"sasaran_program" => [
				"rules" => "required",
				"errors" => [
					"required" => "Sasaran program wajib diisi"
				]
			],
			"tgl_mulai" => [
				"rules" => "required",
				"errors" => [
					"required" => "Tanggal mulai wajib diisi"
				]
			],
			"jam_mulai" => [
				"rules" => "required|regex_match[/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/]",
				"errors" => [
					"required" => "Jam mulai wajib diisi",
					"regex_match" => "Format jam tidak valid"
				]
			],
			"jam_selesai" => [
				"rules" => "required|regex_match[/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/]",
				"errors" => [
					"required" => "Jam selesai wajib diisi",
					"regex_match" => "Format jam tidak valid"
				]
			],
			"biaya" => [
				"rules" => "required",
				"errors" => [
					"required" => "Wajib pilih salah satu"
				]
			],
			"status" => [
				"rules" => "required",
				"errors" => [
					"required" => "Status wajib diisi"
				]
			]
		])) {
			$error = $this->validation->getErrors();
			$gagal = [
				"success" => "false",
				"error" 	=> $error
			];
			return $this->response->setJSON($gagal);
		}

		// Lolos Validasi
		$id = $this->request->getPost('id');
		$nama_program = $this->request->getPost('nama_program');
		$nama_kegiatan = $this->request->getPost('nama_kegiatan');
		$link_pamflet = $this->request->getFile('link_pamflet');
		$pamflet_old = $this->request->getPost('pamflet_old');
		$desc_program = $this->request->getPost('desc_program');
		$link_daftar = $this->request->getPost('link_daftar');
		$sasaran_program = $this->request->getPost('sasaran_program');
		$tgl_mulai = $this->request->getPost('tgl_mulai');
		$tgl_selesai = $this->request->getPost('tgl_selesai');
		$jam_mulai = $this->request->getPost('jam_mulai');
		$jam_selesai = $this->request->getPost('jam_selesai');
		$biaya = $this->request->getPost('biaya');
		$harga_normal = $this->request->getPost('harga_normal');
		$harga_promo = $this->request->getPost('harga_promo');
		$link_jadwal = $this->request->getPost('link_jadwal');
		$link_dokumentasi = $this->request->getPost('link_dokumentasi');
		$status = $this->request->getPost('status');

		// Cek Gambar Pamflet Jika berubah
		$cekPamflet = false;
		$nwPamflet = '';
		if($link_pamflet != "") {
			$nwPamflet = $link_pamflet->getRandomName();
			$simpanPamflet = $link_pamflet->move('assets/img/program', $nwPamflet);
			if ($simpanPamflet) {
				unlink('./assets/img/program/'.$pamflet_old);
				$cekPamflet = true;
			} else {
				$cekPamflet = false;
			}
		} else {
			$nwPamflet = $pamflet_old;
			$cekPamflet = true;
		}
		
		// Simpan perubahan
		$data = [
			'nama_program' => $nama_program,
			'nama_kegiatan' => $nama_kegiatan,
			'link_pamflet' => $nwPamflet,
			'desc_program' => $desc_program,
			'link_daftar' => $link_daftar,
			'sasaran_program' => $sasaran_program,
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'jam_mulai' => $jam_mulai,
			'jam_selesai' => $jam_selesai,
			'biaya' => $biaya,
			'harga_normal' => $harga_normal,
			'harga_promo' => $harga_promo,
			'link_jadwal' => $link_jadwal,
			'link_dokumentasi' => $link_dokumentasi,
			'status' => $status
		];
		
		if ($cekPamflet) {
			$berubah = $this->mdlProgram->update($id, $data);
			if ($berubah) {
				$berhasil = [
					'success' => 'true',
					'message' => 'Berhasil diubah'
				];
				return $this->response->setJSON($berhasil);
			} else {
				$gagal = [
					'success' => 'false',
					'message' => 'Gagal diubah'
				];
				return $this->response->setJSON($gagal);
			}
		} else {
			$gagal = [
				'success' => 'false',
				'message' => 'Gagal diubah'
			];
			return $this->response->setJSON($gagal);
		}
		
	}

	// Hapus Program
	public function hapusprogram($id)
	{
		// Kasih Session
		if (!$this->session->has("is_admin")) {
			return redirect()->route('miadmin/loginadmin');
		}
		$ambilGambar = $this->mdlProgram->where('id',$id)->findcolumn('link_pamflet');
		$hapusGambar = unlink('./assets/img/program/'.$ambilGambar[0]);
		if ($hapusGambar) {
			$hapusProg = $this->mdlProgram->delete($id);
			if ($hapusProg) {
				$berhasil = [
					"success" => "true",
					"message" => "Berhasil dihapus"
				];
				return $this->response->setJSON($berhasil);
			} else {
				$gagal = [
					"success" => "false",
					"message" => "Gagal dihapus"
				];
				return $this->response->setJSON($gagal);
			}
		} else {
			$gagal = [
				"success" => "false",
				"message" => "Gagal dihapus"
			];
			return $this->response->setJSON($gagal);
		}
	}
	
	//--------------------------------------------------------------------

}
