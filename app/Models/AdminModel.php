<?php namespace App\Models;

use CodeIgniter\Model;

class AdminModel extends Model
{
  protected $table = 'tbl_admin';
  protected $primarykey = 'id_admin';
  protected $allowedFields = ['username', 'password'];

  public function cekAdmin() {
    $builder = $this->db->query("SELECT * FROM tbl_admin");
    return $builder;
  }
}
