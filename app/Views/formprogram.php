<?= $this->extend('template/adminlte.php'); ?>

<?= $this->section('additionalcss'); ?>
<link rel="stylesheet" href="<?= base_url('plugins/summernote/summernote-bs4.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('plugins/codemirror/codemirror.css'); ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/add-admin.css'); ?>">
<?= $this->endSection(); ?>

<!-- Sidebar Menu -->
<?= $this->section('sidebarMenu'); ?>
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="<?= base_url('miadmin/homeadmin'); ?>" class="nav-link">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Kumpulan Data
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-table"></i>
        <p>
          Laporan
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="<?= base_url('miadmin/datamis'); ?>" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Laporan MIS 2021</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?= base_url('miadmin/datafinalis'); ?>" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Top Finalis 50 MIS 2021</p>
          </a>
        </li>
        <li class="nav-item">
        <a href="<?= base_url('miadmin/dataprogram'); ?>" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Data Program</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item menu-open">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-edit"></i>
        <p>
          Form
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="<?= base_url('miadmin/formprogram'); ?>" class="nav-link active">
            <i class="far fa-circle nav-icon"></i>
            Form Program
          </a>
        </li>
      </ul>
    </li>
  </ul>
</nav>
<?= $this->endSection(); ?>
<!-- ./Sidebar Menu -->

<?= $this->section('content'); ?>
<!--================ FIXED ALERT =================-->
<div class = 'fixed-alert'></div>
<!--================ END FIXED ALERT =================-->

<div class="row">
  <div class="col-md-12">
    <div class="card card-outline card-info">
      <div class="card-header">
        <h3 class="card-title">Form Program
          <small class="text-muted">Input/Edit Data Program</small>
        </h3>
      </div>
      <div class="card-body">

        <!-- IF ADA DATA -->
        <?php
        if (isset($dtProgram)):
          if (count($dtProgram) > 0):
        ?>
        <form id="formProgram" enctype="multipart/form-data">
          <div class="form-group">
            <label for="nama_program">Nama Program</label>
            <input type="text" class="form-control" id="nama_program" name="nama_program" placeholder="Ex: MIS TALKSHOW #1, PROGRAM PKU, MIS 2021" value="<?= $dtProgram[0]['nama_program'] ?>">
            <div class="invalid-feedback">Error nama program</div>
          </div>
          <div class="form-group">
            <label for="nama_kegiatan">Nama Kegiatan</label>
            <input type="text" class="form-control" id="nama_kegiatan" name="nama_kegiatan" placeholder="Ex: BEDAH PROPOSAL, BUSINESS PLAN 101, PAKET PERINTIS" value="<?= $dtProgram[0]['nama_kegiatan'] ?>">
            <div class="invalid-feedback">Error nama kegiatan</div>
          </div>
          <div class="form-group">
            <label for="link_pamflet">Gambar Pamflet</label>
            <div class="col-12 my-2">
              <img src="<?= base_url('/assets/img/program/'.$dtProgram[0]['link_pamflet']) ?>" alt="gambar-pamflet" class="img-100">
            </div>
            <input type="hidden" name="pamflet_old" value="<?= $dtProgram[0]['link_pamflet'] ?>">
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="link_pamflet" name="link_pamflet">
                <label class="custom-file-label" for="link_pamflet">Unggah file jpeg/jpg/png</label>
              </div>
            </div>
            <small class="text-danger d-none">Error unggah pamflet</small>
          </div>
          <div class="form-group">
            <label for="desc_program">Deskripsi Program</label>
            <textarea name="desc_program" id="desc_program" cols="80" rows="10">
                <?= $dtProgram[0]['desc_program'] ?>
            </textarea>
          </div>
          <div class="form-group">
            <label for="link_daftar">Link Daftar</label>
            <input type="url" class="form-control" id="link_daftar" name="link_daftar" placeholder="https://isiLinkDiSini" value="<?= $dtProgram[0]['link_daftar'] ?>">
            <div class="invalid-feedback">Error link daftar program</div>
          </div>
          <div class="form-group">
            <label for="sasaran_program">Sasaran Program</label>
            <input type="text" class="form-control" id="sasaran_program" name="sasaran_program" placeholder="Ex: Khusus untuk Peserta ..." value="<?= $dtProgram[0]['sasaran_program'] ?>">
            <div class="invalid-feedback">Error sasaran program</div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 form-group">
              <label for="tgl_mulai">Tanggal Mulai</label>
              <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" value="<?= $dtProgram[0]['tgl_mulai'] ?>">
              <div class="invalid-feedback">Error tanggal mulai program</div>
            </div>
            <div class="col-12 col-sm-6 form-group">
              <label for="tgl_selesai">Tanggal Selesai</label>
              <?php 
                $dateStr = strtotime($dtProgram[0]['tgl_selesai']);
              ?>
              <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai" value="<?= ($dateStr !== strtotime('0000-00-00') ? $dtProgram[0]['tgl_selesai'] :  '');
              ?>">
              <div class="invalid-feedback">Error tanggal selesai program</div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 form-group">
              <label for="jam_mulai">Jam Mulai</label>
              <input type="text" class="form-control" id="jam_mulai" name="jam_mulai" placeholder="Hh:Mm" value="<?= $dtProgram[0]['jam_mulai'] ?>" onchange="validateHhMm(this);">
              <div class="invalid-feedback">Error jam mulai program</div>
            </div>
            <div class="col-12 col-sm-6 form-group">
              <label for="jam_selesai">Jam Selesai</label>
              <input type="text" class="form-control" id="jam_selesai" name="jam_selesai" placeholder="Hh:Mm" value="<?= $dtProgram[0]['jam_selesai'] ?>" onchange="validateHhMm(this);">
              <div class="invalid-feedback">Error jam selesai program</div>
            </div>
          </div>
          <div class="form-group">
              <label for="biaya">Biaya</label>
              <select class="form-control" id="biaya" name="biaya" onchange="shBiaya();">
                  <?= ($dtProgram[0]['biaya'] === 'Gratis') ? '<option value="Gratis" selected>Gratis</option>' : '<option value="Gratis">Gratis</option>' ?>
                  <?= ($dtProgram[0]['biaya'] === 'Berbayar') ? '<option value="Berbayar" selected>Berbayar</option>' : '<option value="Berbayar">Berbayar</option>' ?>
              </select>
              <div class="invalid-feedback">Error biaya gaes</div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 d-none form-group">
              <label for="harga_normal">Harga Normal</label>
              <input type="number" class="form-control" id="harga_normal" name="harga_normal" value="<?= $dtProgram[0]['harga_normal'] ?>" min="0">
              <div class="invalid-feedback">Error harga normal gaes</div>
            </div>
            <div class="col-12 col-sm-6 d-none form-group">
              <label for="harga_promo">Harga Promo</label>
              <input type="number" class="form-control" id="harga_promo" name="harga_promo" value="<?= $dtProgram[0]['harga_promo'] ?>" min="0">
              <div class="invalid-feedback">Error harga promo gaes</div>
            </div>
          </div>
          <div class="form-group">
            <label for="link_jadwal">Link Jadwal</label>
            <input type="url" class="form-control" id="link_jadwal" name="link_jadwal" placeholder="Isi Link Jadwal" value="<?= $dtProgram[0]['link_jadwal'] ?>">
            <div class="invalid-feedback">Error link jadwal</div>
          </div>
          <div class="form-group">
            <label for="link_dokumentasi">Link Dokumentasi</label>
            <input type="url" class="form-control" id="link_dokumentasi" name="link_dokumentasi" placeholder="Isi Link Dokumentasi" value="<?= $dtProgram[0]['link_dokumentasi'] ?>">
            <div class="invalid-feedback">Error link dokumentasi</div>
          </div>
          <div class="form-group">
            <label for="status">Status Program</label>
            <select class="form-control" id="status" name="status">
                <?= ($dtProgram[0]['status'] === 'past') ? '<option value="past" selected>Past</option>' : '<option value="past">Past</option>' ?>
                <?= ($dtProgram[0]['status'] === 'on-going') ? '<option value="on-going" selected>On-Going</option>' : '<option value="on-going">On-Going</option>' ?>
                <?= ($dtProgram[0]['status'] === 'upcoming') ? '<option value="upcoming" selected>Upcoming</option>' : '<option value="upcoming">Upcoming</option>' ?>
            </select>
            <div class="invalid-feedback">Error status program</div>
          </div>
          <input type="hidden" name="id" value="<?= $dtProgram[0]['id'] ?>">
        </form>
        <button class="btn btn-primary" id="simpanUbah">Submit</button>

        <?php 
          endif;
        else:
        ?>
        
        <!-- IF TIDAK ADA DATA -->
        <form id="formProgram" enctype="multipart/form-data">
          <div class="form-group">
            <label for="nama_program">Nama Program</label>
            <input type="text" class="form-control" id="nama_program" name="nama_program" placeholder="Ex: MIS TALKSHOW #1, PROGRAM PKU, MIS 2021">
            <div class="invalid-feedback">Error nama program</div>
          </div>
          <div class="form-group">
            <label for="nama_kegiatan">Nama Kegiatan</label>
            <input type="text" class="form-control" id="nama_kegiatan" name="nama_kegiatan" placeholder="Ex: BEDAH PROPOSAL, BUSINESS PLAN 101, PAKET PERINTIS">
            <div class="invalid-feedback">Error nama kegiatan</div>
          </div>
          <div class="form-group">
            <label for="link_pamflet">Gambar Pamflet</label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="link_pamflet" name="link_pamflet">
                <label class="custom-file-label" for="link_pamflet">Unggah file jpeg/jpg/png</label>
              </div>
            </div>
            <small class="text-danger d-none">Error unggah pamflet</small>
          </div>
          <div class="form-group">
            <label for="desc_program">Deskripsi Program</label>
            <textarea name="desc_program" id="desc_program" cols="80" rows="10">
                <p>Tuliskan Deskripsi Program di sini</p>
            </textarea>
          </div>
          <div class="form-group">
            <label for="link_daftar">Link Daftar</label>
            <input type="url" class="form-control" id="link_daftar" name="link_daftar" placeholder="https://isiLinkDiSini">
            <div class="invalid-feedback">Error link daftar program</div>
          </div>
          <div class="form-group">
            <label for="sasaran_program">Sasaran Program</label>
            <input type="text" class="form-control" id="sasaran_program" name="sasaran_program" placeholder="Ex: Khusus untuk Peserta ...">
            <div class="invalid-feedback">Error sasaran program</div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 form-group">
              <label for="tgl_mulai">Tanggal Mulai</label>
              <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai">
              <div class="invalid-feedback">Error tanggal mulai program</div>
            </div>
            <div class="col-12 col-sm-6 form-group">
              <label for="tgl_selesai">Tanggal Selesai</label>
              <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai">
              <div class="invalid-feedback">Error tanggal selesai program</div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 form-group">
              <label for="jam_mulai">Jam Mulai</label>
              <input type="text" class="form-control" id="jam_mulai" name="jam_mulai" placeholder="Hh:Mm" onchange="validateHhMm(this);">
              <div class="invalid-feedback">Error jam mulai program</div>
            </div>
            <div class="col-12 col-sm-6 form-group">
              <label for="jam_selesai">Jam Selesai</label>
              <input type="text" class="form-control" id="jam_selesai" name="jam_selesai" placeholder="Hh:Mm" onchange="validateHhMm(this);">
              <div class="invalid-feedback">Error jam selesai program</div>
            </div>
          </div>
          <div class="form-group">
              <label for="biaya">Biaya</label>
              <select class="form-control" id="biaya" name="biaya" onchange="shBiaya();">
                  <option value="" selected>--Pilih Satu---</option>
                  <option value="Gratis">Gratis</option>
                  <option value="Berbayar">Berbayar</option>
              </select>
              <div class="invalid-feedback">Error biaya gaes</div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 d-none form-group">
              <label for="harga_normal">Harga Normal</label>
              <input type="number" class="form-control" id="harga_normal" name="harga_normal" min="0">
              <div class="invalid-feedback">Error harga normal gaes</div>
            </div>
            <div class="col-12 col-sm-6 d-none form-group">
              <label for="harga_promo">Harga Promo</label>
              <input type="number" class="form-control" id="harga_promo" name="harga_promo" min="0">
              <div class="invalid-feedback">Error harga promo gaes</div>
            </div>
          </div>
          <div class="form-group">
            <label for="link_jadwal">Link Jadwal</label>
            <input type="url" class="form-control" id="link_jadwal" name="link_jadwal" placeholder="Isi Link Jadwal">
            <div class="invalid-feedback">Error link jadwal</div>
          </div>
          <div class="form-group">
            <label for="link_dokumentasi">Link Dokumentasi</label>
            <input type="url" class="form-control" id="link_dokumentasi" name="link_dokumentasi" placeholder="Isi Link Dokumentasi">
            <div class="invalid-feedback">Error link dokumentasi</div>
          </div>
          <div class="form-group">
            <label for="status">Status Program</label>
            <select class="form-control" id="status" name="status">
                <option value="" selected>-- Pilih Status --</option>
                <option value="past">Past</option>
                <option value="on-going">On-Going</option>
                <option value="upcoming">Upcoming</option>
            </select>
            <div class="invalid-feedback">Error status program</div>
          </div>
        </form>
        <button class="btn btn-primary" id="simpan">Submit</button>
      
        <?php endif ?>

      </div>
    </div>
  </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('additionaljs'); ?>
<script src="<?= base_url('plugins/summernote/summernote-bs4.min.js'); ?>"></script>
<script src="<?= base_url('plugins/codemirror/codemirror.js'); ?>"></script>
<script>
  $(document).ready(function() {
    //Summernote Editor
    $("#desc_program").summernote({
      height: 150,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript', 'fontsize', 'fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['misc', ['codeview', 'help', 'fullscreen']]
      ]
    })

    // Form input file bootstrap 4
    $('input[type="file"]').change(function(e){
      var fileName = e.target.files[0].name
      $('.custom-file-label').html(fileName)
    })
})

// Onchange Time Input
function validateHhMm(inputField) {
  let isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

  if (isValid) {
    $('#'+inputField.id).removeClass('is-invalid')
  } else {
    $('#'+inputField.id).addClass('is-invalid')
    $('#'+inputField.id+'~ .invalid-feedback').html('*Waktu Tidak Valid *Format waktu Hh:Mm')    
  }

  return isValid;
}

// SHOW HIDE HARGA BAYAR
function shBiaya() {
  let pilihan = $('#biaya').val()
  if (pilihan == 'Gratis') {
    $('#harga_promo').val('')
    $('#harga_normal').val('')
    $('div:has(> #harga_promo)').addClass('d-none')
    $('div:has(> #harga_normal)').addClass('d-none')
  } else if(pilihan == 'Berbayar') {
    //Tampilkan Input Harga
    $('div:has(> #harga_promo)').removeClass('d-none')
    $('div:has(> #harga_normal)').removeClass('d-none')
    // $('#harga_normal').attr('required', true)
  }
}

// KUMPULAN ALERT
  // = AL FIXED
  // ------------------ FIXED ALERT
  function alFixedBerhasil(pesan) {
    let alBerhasil = `
    <div class="alert alert-success shadow alert-dismissible fade show" role="alert">
    <div id="text-berhasil">${pesan}</div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    `
    $('.fixed-alert').html(alBerhasil)
    $('.fixed-alert').html(alBerhasil).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }

  function alFixedGagal(pesan) {
    let alGagal = `
    <div class="alert alert-danger shadow alert-dismissible fade show" role="alert">
    <div id="text-berhasil">${pesan}</div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    `
    $('.fixed-alert').html(alGagal).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }
  // -------------------------- END FIXED ALERT

// SIMPAN PROGRAM
$('#simpan').on("click", function() {
  // harga normal jika berbayar
  let pilihan = $('#biaya').val()
  let harganor = $('#harga_normal').val()
  if (pilihan == 'Berbayar' && harganor == '') {
    $('#harga_normal').addClass('is-invalid')
    $('#harga_normal + .invalid-feedback').html('Harga normal wajib diisi')
  } else {
    $('#harga_normal').removeClass('is-invalid')
    $.ajax({
      type: "post",
      url: "<?= base_url('/miadmin/inputprogram'); ?>",
      contentType: false,
      cache: false,
      processData: false,
      data: new FormData($('#formProgram')[0]),
      beforeSend: function() {
        $("#simpan").addClass('bg-secondary')
        $("#simpan").attr('disabled', true)
      },
      success: function(data) {
        $("#simpan").removeClass('bg-secondary')
        $("#simpan").attr('disabled', false)
        // console.log(data)
        if (data.success == 'false') {
          
          // = Nama_Program
          if (data.error.nama_program == null) {
            // Hapus invalid
            $('#nama_program').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#nama_program').addClass('is-invalid')
            $('#nama_program + .invalid-feedback').html(data.error.nama_program)
          }

          //  = Nama_Kegiatan
          if (data.error.nama_kegiatan == null) {
            // Hapus invalid
            $('#nama_kegiatan').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#nama_kegiatan').addClass('is-invalid')
            $('#nama_kegiatan + .invalid-feedback').html(data.error.nama_kegiatan)
          }

          // = Gambar Pamflet
          if (data.error.link_pamflet == null) {
            // Hapus invalid
            $('#link_pamflet').removeClass('is-invalid')
            $('div:has(> .custom-file) + small').addClass('d-none')
          } else {
            // Munculkan invalid
            $('#link_pamflet').addClass('is-invalid')
            $('div:has(> .custom-file) + small').removeClass('d-none')
            $('div:has(> .custom-file) + .text-danger').html(data.error.link_pamflet)
          }

          // = Link Daftar
          if (data.error.link_daftar == null) {
            // Hapus invalid
            $('#link_daftar').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#link_daftar').addClass('is-invalid')
            $('#link_daftar + .invalid-feedback').html(data.error.link_daftar)
          }

          // = Sasaran program
          if (data.error.sasaran_program == null) {
            // Hapus invalid
            $('#sasaran_program').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#sasaran_program').addClass('is-invalid')
            $('#sasaran_program + .invalid-feedback').html(data.error.sasaran_program)
          }

          // = Tanggal mulai
          if (data.error.tgl_mulai == null) {
            // Hapus invalid
            $('#tgl_mulai').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#tgl_mulai').addClass('is-invalid')
            $('#tgl_mulai + .invalid-feedback').html(data.error.tgl_mulai)
          }

          // = Jam mulai
          if (data.error.jam_mulai == null) {
            // Hapus invalid
            $('#jam_mulai').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#jam_mulai').addClass('is-invalid')
            $('#jam_mulai + .invalid-feedback').html(data.error.jam_mulai)
          }

          // = Jam selesai
          if (data.error.jam_selesai == null) {
            // Hapus invalid
            $('#jam_selesai').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#jam_selesai').addClass('is-invalid')
            $('#jam_selesai + .invalid-feedback').html(data.error.jam_selesai)
          }

          // = Biaya
          if (data.error.biaya == null) {
            // Hapus invalid
            $('#biaya').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#biaya').addClass('is-invalid')
            $('#biaya + .invalid-feedback').html(data.error.biaya)
          }

          // = Status
          if (data.error.status == null) {
            // Hapus invalid
            $('#status').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#status').addClass('is-invalid')
            $('#status + .invalid-feedback').html(data.error.status)
          }
        } else {
          // Jika Sukses Maka ...
          $('#nama_program').removeClass('is-invalid')
          $('#nama_kegiatan').removeClass('is-invalid')
          $('#link_pamflet').removeClass('is-invalid')
          $('div:has(> .custom-file) + small').addClass('d-none')
          $('#link_daftar').removeClass('is-invalid')
          $('#sasaran_program').removeClass('is-invalid')
          $('#tgl_mulai').removeClass('is-invalid')
          $('#jam_mulai').removeClass('is-invalid')
          $('#jam_selesai').removeClass('is-invalid')
          $('#biaya').removeClass('is-invalid')
          $('#status').removeClass('is-invalid')
          // ==== Alert berhasil disimpan
          alFixedBerhasil(data.message)
          // === Reset Form
          $('#formProgram')[0].reset();
          $("#link_pamflet").val(null);
          $('.custom-file-label').html('Unggah file jpeg/jpg/png');
          $('#desc_program').summernote('code', '<p>Tuliskan Deskripsi Program di sini</p>')
        }
      },
      error: function(err) {
        console.error(err);
        alFixedGagal(err.responseJSON.message)
        $("#simpan").attr('disabled', false)
      }
    })
  }
})
// END SIMPAN PROGRAM

// SIMPAN UBAH
$('#simpanUbah').on("click", function() {
  // harga normal jika berbayar
  let pilihan = $('#biaya').val()
  let harganor = $('#harga_normal').val()
  if (pilihan == 'Berbayar' && harganor == '') {
    $('#harga_normal').addClass('is-invalid')
    $('#harga_normal + .invalid-feedback').html('Harga normal wajib diisi')
  } else {
    $('#harga_normal').removeClass('is-invalid')
    $.ajax({
      type: "post",
      url: "<?= base_url('/miadmin/prsubahprogram'); ?>",
      contentType: false,
      cache: false,
      processData: false,
      data: new FormData($('#formProgram')[0]),
      beforeSend: function() {
        $("#simpanUbah").addClass('bg-secondary')
        $("#simpanUbah").attr('disabled', true)
      },
      success: function(data) {
        $("#simpanUbah").removeClass('bg-secondary')
        $("#simpanUbah").attr('disabled', false)
        // console.log(data)
        if (data.success == 'false') {
          
          // = Nama_Program
          if (data.error.nama_program == null) {
            // Hapus invalid
            $('#nama_program').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#nama_program').addClass('is-invalid')
            $('#nama_program + .invalid-feedback').html(data.error.nama_program)
          }

          //  = Nama_Kegiatan
          if (data.error.nama_kegiatan == null) {
            // Hapus invalid
            $('#nama_kegiatan').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#nama_kegiatan').addClass('is-invalid')
            $('#nama_kegiatan + .invalid-feedback').html(data.error.nama_kegiatan)
          }

          // = Gambar Pamflet
          if (data.error.link_pamflet == null) {
            // Hapus invalid
            $('#link_pamflet').removeClass('is-invalid')
            $('div:has(> .custom-file) + small').addClass('d-none')
          } else {
            // Munculkan invalid
            $('#link_pamflet').addClass('is-invalid')
            $('div:has(> .custom-file) + small').removeClass('d-none')
            $('div:has(> .custom-file) + .text-danger').html(data.error.link_pamflet)
          }

          // = Link Daftar
          if (data.error.link_daftar == null) {
            // Hapus invalid
            $('#link_daftar').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#link_daftar').addClass('is-invalid')
            $('#link_daftar + .invalid-feedback').html(data.error.link_daftar)
          }

          // = Sasaran program
          if (data.error.sasaran_program == null) {
            // Hapus invalid
            $('#sasaran_program').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#sasaran_program').addClass('is-invalid')
            $('#sasaran_program + .invalid-feedback').html(data.error.sasaran_program)
          }

          // = Tanggal mulai
          if (data.error.tgl_mulai == null) {
            // Hapus invalid
            $('#tgl_mulai').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#tgl_mulai').addClass('is-invalid')
            $('#tgl_mulai + .invalid-feedback').html(data.error.tgl_mulai)
          }

          // = Jam mulai
          if (data.error.jam_mulai == null) {
            // Hapus invalid
            $('#jam_mulai').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#jam_mulai').addClass('is-invalid')
            $('#jam_mulai + .invalid-feedback').html(data.error.jam_mulai)
          }

          // = Jam selesai
          if (data.error.jam_selesai == null) {
            // Hapus invalid
            $('#jam_selesai').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#jam_selesai').addClass('is-invalid')
            $('#jam_selesai + .invalid-feedback').html(data.error.jam_selesai)
          }

          // = Biaya
          if (data.error.biaya == null) {
            // Hapus invalid
            $('#biaya').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#biaya').addClass('is-invalid')
            $('#biaya + .invalid-feedback').html(data.error.biaya)
          }

          // = Status
          if (data.error.status == null) {
            // Hapus invalid
            $('#status').removeClass('is-invalid')
          } else {
            // Munculkan invalid
            $('#status').addClass('is-invalid')
            $('#status + .invalid-feedback').html(data.error.status)
          }
        } else {
          // Jika Sukses Maka ...
          $('#nama_program').removeClass('is-invalid')
          $('#nama_kegiatan').removeClass('is-invalid')
          $('#link_pamflet').removeClass('is-invalid')
          $('div:has(> .custom-file) + small').addClass('d-none')
          $('#link_daftar').removeClass('is-invalid')
          $('#sasaran_program').removeClass('is-invalid')
          $('#tgl_mulai').removeClass('is-invalid')
          $('#jam_mulai').removeClass('is-invalid')
          $('#jam_selesai').removeClass('is-invalid')
          $('#biaya').removeClass('is-invalid')
          $('#status').removeClass('is-invalid')
          // ==== Alert berhasil disimpan
          alFixedBerhasil(data.message)
          // === Reset Form
          $('#formProgram')[0].reset();
          $("#link_pamflet").val(null);
          $('.custom-file-label').html('Unggah file jpeg/jpg/png');
          $('#desc_program').summernote('code', '<p>Tuliskan Deskripsi Program di sini</p>')
        }
      },
      error: function(err) {
        console.error(err);
        alFixedGagal(err.responseJSON.message)
        $("#simpan").attr('disabled', false)
      }
    })
  }
})
// END SIMPAN UBAH
</script>
<?= $this->endSection() ?>