<?= $this->extend('template_merintis/merintis.php'); ?>

<?= $this->section('head-title'); ?>
<title>Merintis Indonesia</title>
<?= $this->endSection(); ?>
<!-- SEO -->
<?= $this->section('meta-description'); ?>
<meta name="description" content="Merintis Indonesia - Merintis Indonesia adalah ekosistem kreatif muda/i daerah untuk saling terhubung, berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.">
<?= $this->endSection(); ?>

<?= $this->section('addCSS'); ?>
<link rel="stylesheet" href="<?= base_url('assets/css/home.css'); ?>">
<?= $this->endSection(); ?>

<?php
  $isLogin = false;
  $idAkun = '';
  $token = '';
  if(isset($_SESSION['is_login'])) {
    $isLogin = $_SESSION['is_login'];
    $idAkun = $_SESSION['id_akun'];
    $token = $_SESSION['token'];
  } else if(isset($_COOKIE['is_login'])) {
    $isLogin = $_COOKIE['is_login'];
    $idAkun = $_COOKIE['id_akun'];
    $token = $_COOKIE['token'];
  } 
?>

<!--================ Header Menu Area start =================-->
<?= $this->section('header'); ?>
<div class="bg-coffee">
  <header>
    <div class="mis-header">
      <div class="mis-header-logo-100">
        <a href="#">
          <img src="<?= base_url('assets/img/Logo-MI.png'); ?>" alt="logo-mi">
        </a>
      </div>
    </div>
    <!-- BATAS NAVBAR -->
    <nav class="navbar navbar-expand-sm navbar-dark">
      <div class="mx-auto">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMis" aria-controls="navbarMis" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="navbarMis">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item">
            <a class="nav-link" href="#beranda">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#tentang">Tentang</a>
          </li>
          <!-- PROGRAM LINK-->
          <?php
            if($isLogin) {
              echo '<li class="nav-item">
                      <a class="nav-link" href="'.base_url('/program').'">Program</a>
                    </li>';
            } else {
              echo '<li class="nav-item">
                      <a class="nav-link" href="javascript:void(0)" onclick="followProgram()">Program</a>
                    </li>';
            }
          ?>
          <!-- END PROGRAM LINK-->
          <li class="nav-item">
            <a class="nav-link" href="#team">Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#content">Content</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://info-merintisindonesia.medium.com/">Blog</a>
          </li>
          <li class="nav-item">
            <!-- Login/Logout -->
            <div id="link-log">
              <?= ($isLogin ? '<a class="nav-link" href="javascript:void(0)" onclick="logout()">Sign Out</a>' : '<a class="nav-link" id="btnSignIn" href="'.base_url('/signin').'">Sign In</a>'); ?>
            </div>
            <!-- End Login/Logout -->
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <?= $this->endSection(); ?>
  <!--================ END Header Menu Area =================-->

  <?= $this->section('content'); ?>
  <!--================ FIXED ALERT =================-->
  <div class='fixed-alert'></div>
  <!--================ END FIXED ALERT =================-->

  <!--================ Section Area =================-->
  <!-- ============ SECTION BERANDA -->
  <section id="beranda">
    <div class="container text-center">
      <h1 class="roboto-condensed text-white h1">Mau Bangun Bisnis Secara Praktis?</h1>
      <h1 class="roboto-condensed text-white h1 mb-4">Wujudkan bersama Merintis Indonesia!</h1>
      <div class="d-flex flex-wrap justify-content-center">
      <!-- BTN PROGRAM AND SIGNUP-->
      <?php
            if($isLogin) {
              echo '<a class="btn-kuning link-none my-2 mx-3" href="'.base_url('/program').'">Ikuti Program</a>';
            } else {
              echo '
              <a class="btn-kuning link-none my-2 mx-3" href="javascript:void(0)" onclick="followProgram()">Ikuti Program</a>
              <a href="'.base_url('/signup').'" class="btn-kuning link-none my-2 mx-3">Sign Up</a>
              ';
            }
          ?>  
      <!-- END BTN PROGRAM AND SIGNUP-->        
      </div>
      <a href="#contact">
        <div class="box-talks">
          <span class="roboto-condensed">Talk <br>To Us!</span>
          <img src="<?= base_url('/assets/img/talk-to-us.png'); ?>" alt="talk-to-us">
        </div>
      </a>
    </div>
  </section>
</div>

<!-- ============ SECTION TENTANG -->
<section id="tentang">
  <div class="container text-center mt-48">
    <figure class="px-48">
      <img src="<?= base_url("/assets/img/logo-mis-color.png"); ?>" alt="logo-mis" class="img-100 mb-4">
      <figcaption class="text-18 let-space-08 text-left">
        <p><b>Merintis Indonesia</b> adalah ekosistem kreatif muda/i daerah untuk saling terhubung,
          berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.</p>
      </figcaption>
    </figure>

    <div class="d-flex flex-wrap justify-content-center roboto-condensed">
      <div class="mx-3 pt-2">
        <h2 class="text-gold"><span class="countA">40</span>+</h2>
        <h5 class="text-green">Communities</h5>
      </div>
      <div class="mx-3 pt-2">
        <h2 class="text-gold"><span class="countA">10</span>+</h2>
        <h5 class="text-green">Cities</h5>
      </div>
      <div class="mx-3 pt-2">
        <h2 class="text-gold"><span class="countA">30</span>+</h2>
        <h5 class="text-green">Universities</h5>
      </div>
    </div>

    <div class="d-flex flex-wrap justify-content-around roboto-condensed mt-2">
      <div class="mt-1 mx-2 pt-2">
        <h1 class="text-gold"><span class="countB">900</span>+<span class="text-green text-32">/16 Talks</span></h2>
      </div>
      <div class="mt-1 mx-2 pt-2">
        <h1 class="text-gold"><span class="countB">200</span>+<span class="text-green text-32"> Intern</span></h2>
      </div>
    </div>

    <p class="roboto-condensed text-green text-18">OUR PARTNERS</p>
    <div class="d-flex flex-wrap justify-content-center">
      <img src="<?= base_url('/assets/img/Picture11.png'); ?>" alt="madiun-muda" class="img-50 mx-3">
      <img src="<?= base_url('/assets/img/Picture24.png'); ?>" alt="maesa-group" class="img-50 mx-3">
    </div>
  </div>
</section>

<!-- ============ SECTION TEAM -->
<section id="team" class="mt-120">
  <div class="container roboto-condensed text-center">
    <h1 class="text-green">FOUNDER</h1>
    <h5 class="text-gold">MERINTIS INDONESIA</h5>
    <div class="d-flex flex-wrap justify-content-center mt-3">
      <img src="<?= base_url('/assets/img/ceo-mi.png'); ?>" alt="CEO Merintis Indonesia" class="img-295 mx-4 my-1">
      <img src="<?= base_url('/assets/img/cso-mi.png'); ?>" alt="CSO Merintis Indonesia" class="img-295 mx-4 my-1">
      <img src="<?= base_url('/assets/img/cmo-mi.png'); ?>" alt="CMO Merintis Indonesia" class="img-295 mx-4 my-1">
    </div>

    <!-- VIP TEAM -->
    <h1 class="text-green mt-64">Internship Program #01</h1>
      <div class="d-flex flex-wrap justify-content-left mt-3">
        <!-- BARIS 1 -->
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip1.png'); ?>" alt="Vip1" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Miftahul Fauza R</span>
            <span class="block text-gold text-12 mr-2">Universitas PGRI Madiun</span>
            <span class="block roboto text-grey text-10 mr-2">Teknik Informatika</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip2.png'); ?>" alt="Vip2" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Jahfal Uno S L</span>
            <span class="block text-gold text-12 mr-2">Universitas Gunadarma</span>
            <span class="block roboto text-grey text-10 mr-2">Teknik Informatika</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip3.png'); ?>" alt="Vip3" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Jeffrens Tanadi</span>
            <span class="block text-gold text-12 ml-4">Binus University</span>
            <span class="block roboto text-grey text-10 ml-4">Business Information Technology</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip4.png'); ?>" alt="Vip4" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Pedro Ozora</span>
            <span class="block text-gold text-12 ml-4">Institut Teknologi Del</span>
            <span class="block roboto text-grey text-10 ml-4">Teknologi Komputer</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip5.png'); ?>" alt="Vip5" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Audrey Dian F</span>
            <span class="block text-gold text-12 ml-4">Institut Teknologi Bandung</span>
            <span class="block roboto text-grey text-10 ml-4">Matematika</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip6.png'); ?>" alt="Vip6" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Mutia Mahhal</span>
            <span class="block text-gold text-12 ml-4">Universitas Andalas</span>
            <span class="block roboto text-grey text-10 ml-4">Ilmu Komunikasi</span>
          </div>
        </div>
        <div class="card border-none mr-1">
          <img src="<?= base_url('/assets/img/Vip7.png'); ?>" alt="Vip7" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Firdausyi Nuzulah</span>
            <span class="block text-gold text-12 ml-4">Politeknik Negeri Jakarta</span>
            <span class="block roboto text-grey text-10 ml-4">Manajemen Keuangan</span>
          </div>
        </div>

        <!-- BARIS 2 -->
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip8.png'); ?>" alt="Vip8" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Dedi</span>
            <span class="block text-gold text-12 mr-2">Universitas Padjajaran</span>
            <span class="block roboto text-grey text-10 mr-2">Sastra Indonesia</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip9.png'); ?>" alt="Vip9" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Dian Rahmadani L</span>
            <span class="block text-gold text-10 mr-2">Universitas Negeri Surabaya</span>
            <span class="block roboto text-grey text-10 mr-2">Ilmu Komunikasi</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip10.png'); ?>" alt="Vip10" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Cindy Marilyn C</span>
            <span class="block text-gold text-10 mr-2">Universitas Darma Persada</span>
            <span class="block roboto text-grey text-10 mr-2">Sastra Jepang</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip11.png'); ?>" alt="Vip11" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Eldiastari Putri A</span>
            <span class="block text-gold text-12 mr-2">Universitas Gunadarma</span>
            <span class="block roboto text-grey text-10 mr-2">Ilmu Komunikasi</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip12.png'); ?>" alt="Vip12" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Adhini Bintang P</span>
            <span class="block text-gold text-12 mr-2">Universitas Diponegoro</span>
            <span class="block roboto text-grey text-10 mr-2">Administrasi Bisnis</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip13.png'); ?>" alt="Vip13" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Chenny Chang</span>
            <span class="block text-gold text-8 mr-2">Institut Pariwisata dan Bisnis Internasional</span>
            <span class="block roboto text-grey text-10 mr-2">Hospitality Management</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip14.png'); ?>" alt="Vip14" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Al-Hanaan</span>
            <span class="block text-gold text-12 mr-2">Universitas Terbuka</span>
            <span class="block roboto text-grey text-10 mr-2">Ekonomi Pembangunan</span>
          </div>
        </div>

        <!-- DISEMBUNYIKAN -->
          <!-- BARIS 3 -->
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip15.png'); ?>" alt="Vip15" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Aldo Putra</span>
              <span class="block text-gold text-12 ml-4">Universitas Gadjah Mada</span>
              <span class="block roboto text-grey text-10 ml-4">Teknik Fisika</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip16.png'); ?>" alt="Vip16" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Leonardo Anthony</span>
              <span class="block text-gold text-12 ml-4">Universitas Gadjah Mada</span>
              <span class="block roboto text-grey text-10 ml-4">Teknik Kimia</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip17.png'); ?>" alt="Vip17" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Dwi Khoiriyah</span>
              <span class="block text-gold text-12 ml-4">Politeknik Negeri Jakarta</span>
              <span class="block roboto text-grey text-10 ml-4">Administrasi Bisnis Terapan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip18.png'); ?>" alt="Vip18" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Raihan Ariq</span>
              <span class="block text-gold text-12 ml-4">Binus University</span>
              <span class="block roboto text-grey text-10 ml-4">IT dan Statistika</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip19.png'); ?>" alt="Vip19" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Novitriana Gadis</span>
              <span class="block text-gold text-12 ml-4">Telkom University</span>
              <span class="block roboto text-grey text-10 ml-4">Sistem Informasi</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip20.png'); ?>" alt="Vip20" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Asna Af'idatul</span>
              <span class="block text-gold text-12 ml-4">Universitas Brawijaya</span>
              <span class="block roboto text-grey text-10 ml-4">Ilmu Administrasi Bisnis</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip21.png'); ?>" alt="Vip21" class="img-180  my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Yuwinda Meysella</span>
              <span class="block text-gold text-12 mr-2">Universitas Negeri Malang</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>

          <!-- BARIS 4 -->
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip22.png'); ?>" alt="Vip22" class="img-180  my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Mutiara Suci R</span>
              <span class="block text-gold text-12 mr-2">IKOPIN</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip23.png'); ?>" alt="Vip23" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Jovita Aurelia</span>
              <span class="block text-gold text-10 mr-2">Universitas Katolik Atma Jaya</span>
              <span class="block roboto text-grey text-10 mr-2">Teknologi Pangan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip24.png'); ?>" alt="Vip24" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Bagus Pamungkas</span>
              <span class="block text-gold text-12 mr-2">UNMUH Prof Dr Hamka</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip26.png'); ?>" alt="Vip26" class="img-180  my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Reni Ridayanti</span>
              <span class="block text-gold text-12 mr-2">Universitas Negeri Malang</span>
              <span class="block roboto text-grey text-10 mr-2">Ekonomi Pembangunan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip27.png'); ?>" alt="Vip27" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Maasa Sunreza M</span>
              <span class="block text-gold text-12 ml-4">Universitas Airlangga</span>
              <span class="block roboto text-grey text-10 ml-4">Fakultas Kedokteran</span>
            </div>
          </div>
          <!-- BARIS 5 -->
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip28.png'); ?>" alt="Vip28" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Asmawi Anwar</span>
              <span class="block text-gold text-12 ml-4">Universitas PGRI Madiun</span>
              <span class="block roboto text-grey text-10 ml-4">Pendidikan Guru Sekolah Dasar</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip29.png'); ?>" alt="Vip29" class="img-180  my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Rr. Nektara Titan</span>
              <span class="block text-gold text-12 ml-4">Universitas Jember</span>
              <span class="block roboto text-grey text-10 ml-4">Pendidikan Dokter Gigi</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip30.png'); ?>" alt="Vip30" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Emi Yanti P</span>
              <span class="block text-gold text-12 mr-2">Universitas Diponegoro</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip31.png'); ?>" alt="Vip31" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Jessica Ananda</span>
              <span class="block text-gold text-12 mr-2">President University</span>
              <span class="block roboto text-grey text-10 mr-2">Teknik Industri</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip32.png'); ?>" alt="Vip32" class="img-180  my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Mujahid Najib</span>
              <span class="block text-gold text-12 mr-2">Universitas Indonesia</span>
              <span class="block roboto text-grey text-10 mr-2">Geofisika</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip33.png'); ?>" alt="Vip33" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Haryadi Bagus</span>
              <span class="block text-gold text-12 ml-4">Universitas Brawijaya</span>
              <span class="block roboto text-grey text-10 ml-4">Peternakan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip34.png'); ?>" alt="Vip34" class="img-180  my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Syachfara N</span>
              <span class="block text-gold text-12 ml-4">Universitas Bina Nusantara</span>
              <span class="block roboto text-grey text-10 ml-4">Public Relations</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip35.png'); ?>" alt="Vip35" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Diva Zulfania T</span>
              <span class="block text-gold text-12 mr-2">Universitas Negeri Malang</span>
              <span class="block roboto text-grey text-10 mr-2">Teknik Mesin</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip36.png'); ?>" alt="Vip36" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Syafira Nur Alifah</span>
              <span class="block text-gold text-12 mr-2">Universitas Padjajaran</span>
              <span class="block roboto text-grey text-10 mr-2">Hubungan Internasional</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip37.png'); ?>" alt="Vip37" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Anggraeni Dwi A</span>
              <span class="block text-gold text-12 mr-2">Griffith University Australia</span>
              <span class="block roboto text-grey text-10 mr-2">Hubungan Internasional</span>
            </div>
          </div>
        <!-- END DISEMBUNYIKAN -->

    </div>
    <!-- BUTTON_SELENGKAPNYA -->
    <button type="button" class="button-kuning border-none roboto-condensed let-space-08 my-4" data-toggle="collapse" data-target=".hide-vip" id="btnMore">Selengkapnya</button>
</section>

<!-- ============ SECTION CONTENT -->
<section id="content" class="my-4">
  <div class="container roboto-condensed text-center">
  <h1 class="text-green mt-64">Our Content</h1>
      <div class="d-flex flex-wrap justify-content-center mt-3">  
      <div class="card-content mx-2 my-2">
          <a href="https://www.instagram.com/merintis.indonesia/guide/mis-talkshow/17857178675481615/" target="_blank">
              <div class="overlay-content text-white text-left p-4">
                <h6 class="text-content">MIS TALKSHOW</h6>
              </div>
              <img src="<?= base_url('/assets/img/mistalkshow.png'); ?>" alt="mistalkshow" class="img-295">
          </a>
        </div>
        <div class="card-content  mx-2 my-2">
            <a href="https://www.instagram.com/merintis.indonesia/guide/podcast-merintis-indonesia/18146108674127830/" target="_blank">
              <div class="overlay-content text-white text-left p-4">
                <h6 class="text-content">PODCAST MERINTIS INDONESIA</h6>
              </div>
            <img src="<?= base_url('/assets/img/podcast.png'); ?>" alt="podcast" class="img-295">
          </a>
        </div>
        <div class="card-content  mx-2 my-2">
            <a href="https://www.instagram.com/merintis.indonesia/guide/berita-bisnis-merintis-indonesia/17896639051763833/" target="_blank">
            <div class="overlay-content text-white text-left p-4">
              <h6 class="text-content">BERITA BISNIS MERINTIS INDONESIA</h6>
            </div>
            <img src="<?= base_url('/assets/img/berita_bisnis.png'); ?>" alt="berita-bisnis" class="img-295">
          </a>
        </div>
        <div class="card-content  mx-2 my-2">
          <a href="https://www.instagram.com/merintis.indonesia/guide/intern-talks/18152822482113725/" target="_blank">
            <div class="overlay-content text-white text-left p-4">
              <h6 class="text-content">INTERN TALKS</h6>
            </div>
            <img src="<?= base_url('/assets/img/intern_talk.png'); ?>" alt="intern-talk" class="img-295">
          </a>
        </div>
      </div>
  </div>
</section>
<!-- ============ END SECTION CONTENT -->

<!-- ============ SECTION CONTACT US -->
<section id="contact" class="text-center">
  <div class="mis-header-logo-100">
    <img src="<?= base_url('assets/img/mis-white.png'); ?>" alt="logo-mis">
  </div>
  <h3 class="roboto-condensed text-white mt-2">MERINTIS INDONESIA</h3>
  <h3 class="roboto-condensed text-white mb-4">BERSAMA PENGUSAHA MUDA</h3>
  <span class="btn-hijau px-4">Contact Us</span>
  <div class="d-flex justify-content-center flex-nowrap mt-4">
    <span class="sosmed">
      <a class="overlay" href="https://wa.me/+6285785036770" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed1.svg'); ?>" alt="WA">
    </span>
    <span class="sosmed">
      <a class="overlay" href="https://www.facebook.com/merintisindonesia/" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed2.svg'); ?>" alt="fb">
    </span>
    <span class="sosmed">
      <a class="overlay" href="https://open.spotify.com/show/2v1GGLrQU076JQ9LZTLi52?si=nf_wLH8JTUOSOcStfSdZ-w" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed3.svg'); ?>" alt="spotify">
    </span>
  </div>
  <div class="d-flex justify-content-center flex-nowrap mt-2">
    <span class="sosmed">
      <a class="overlay" href="https://www.instagram.com/merintis.indonesia/" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed4.svg'); ?>" alt="IG">
    </span>
    <span class="sosmed">
      <a class="overlay" href="https://twitter.com/MerintisID?s=08" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed5.svg'); ?>" alt="twitter">
    </span>
    <span class="sosmed">
      <a class="overlay" href="https://vt.tiktok.com/ZStJL8Gp/" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed6.svg'); ?>" alt="tiktok">
    </span>
  </div>
  <div class="d-flex justify-content-center flex-nowrap mt-2">
    <span class="sosmed">
      <a class="overlay" href="mailto:info.merintisindonesia@gmail.com" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed7.svg'); ?>" alt="email">
    </span>
    <span class="sosmed">
      <a class="overlay" href="https://www.linkedin.com/company/merintis-indonesia" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed8.svg'); ?>" alt="linkedin">
    </span>
    <span class="sosmed">
      <a class="overlay" href="https://info-merintisindonesia.medium.com/" target="_blank"></a>
      <img src="<?= base_url('assets/img/sosmed9.svg'); ?>" alt="link">
    </span>
  </div>
</section>
<!--================ END Section Area =================-->
<?= $this->endSection(); ?>

<!--================ Footer Area =================-->
<?= $this->section('footer'); ?>
<footer class="roboto py-2">
  <div class="container text-center">
    <span class="text-grey">Copyright &copy; <span id="yearNow">tahun</span> All rights reserved</span>
  </div>
</footer>
<?= $this->endSection(); ?>
<!--================ END Footer Area =================-->

<?= $this->section('addScript'); ?>
<script>
  // Tambahan Script

  // SMOOTH SCROLL
  $(document).ready(function() {
    $("a").on("click", function(event) {
      if (this.hash !== "") {
        event.preventDefault();
        let hash = this.hash;

        $("html, body").animate({
          scrollTop: $(hash).offset().top
        }, 800, function() {
          window.location.hash = hash;
        });
      }
    });
  });

  // ----------- CEK JIKA DARI HALAMAN DAFTAR -----------
  $(document).ready(function() {
    let params = (new URL(document.location)).searchParams;
    let stat = parseInt(params.get('stat'));
    if (stat) {
      $("#modalLogin").modal('toggle');
    }
  });
  // ----------- END CEK JIKA DARI HALAMAN DAFTAR -----------

  // SHOW NAVBAR FIXED
  window.onscroll = changeNav;

  function changeNav() {
    let navbar = $('nav');
    if (window.pageYOffset > 90) {
      navbar.addClass('navbar-fixed')
    } else {
      navbar.removeClass('navbar-fixed');
    }
  }

  // GET YEAR NOW
  let date = new Date();
  let now = date.getFullYear();
  document.getElementById("yearNow").innerHTML = now;

  // COUNT UP ANIMATION
  $(document).ready(function() {
    countUp(".countA");
    countUp(".countB");

    function countUp(target) {
      let a = 0;
      $(window).scroll(function() {

        let oTop = $(target).offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
          $(target).each(function() {
            let $this = $(this);
            jQuery({
              Counter: 0
            }).animate({
              Counter: $this.text()
            }, {
              duration: 1250,
              easing: 'swing',
              step: function() {
                $this.text(Math.ceil(this.Counter));
              }
            });
          });
          a = 1;
        }
      });
    }
  });

  // ==================== LOGOUT ===================
  function logout() {
    //console.log("Berhasil logout");
    // --------------- LOGOUT -------------------
    $(this).on("click", function() {
      // ------ MENGHAPUS SESSION DI DATABASE
      $('#link-log').addClass("active");
      let session_id = "<?= $idAkun ?>";
      let session_token = "<?= $token ?>";
      $.ajax({
        type: "post",
        url: "<?= base_url('/akun/hapussession'); ?>",
        data: {
          "id_akun": session_id
        },
        success: function() {
          window.location.replace("<?= base_url(); ?>");
        }
      })
      // ------ END MENGHAPUS SESSION DI DATABASE
    })
  }
  // ==================== END LOGOUT ===============

  // ------------------ FIXED ALERT
  function alFixedBerhasil(pesan) {
    let alBerhasil = `
      <div class="alert alert-success alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
    $('.fixed-alert').html(alBerhasil)
    $('.fixed-alert').html(alBerhasil).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }

  function alFixedGagal(pesan) {
    let alGagal = `
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
    $('.fixed-alert').html(alGagal).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }
  // -------------------------- END FIXED ALERT

  // ------------------ BTN COLLAPSE VIP ----------------
  $('.hide-vip').on('show.bs.collapse', function() {
    $('#btnMore').html('Sedikit')
  })
  $('.hide-vip').on('hide.bs.collapse', function() {
    $('#btnMore').html('Selengkapnya')
  })
  // ------------------ END BTN COLLAPSE VIP ----------------

  // FOLLOW PROGRAM
      function followProgram() {
        let isLogin = '<?= ($isLogin ? $isLogin: 'false') ?>'
        if(isLogin === 'false') {
          alFixedGagal('Silahkan Login Dahulu!')
        }
      }
  // END FOLLOW PROGRAM

  // SHOW MESSAGE SUCCESS LOGIN
      if(sessionStorage.getItem("login") === "berhasil") {
        alFixedBerhasil('Login Berhasil')
        sessionStorage.removeItem("login")
      }
  // END SHOW MESSAGE SUCCESS LOGIN

  // -------- SCROLL SPY
  let beranda = document.querySelector("#beranda");
  let tentang = document.querySelector("#tentang");
  let team = document.querySelector("#team");
  let content = document.querySelector("#content");
  let contact = document.querySelector("#contact");

  window.addEventListener("scroll", () => {
    var windo = window.pageYOffset;
    if (beranda.offsetTop <= windo && tentang.offsetTop - 100 > windo) {
      $(".nav-item").eq(1).removeClass("active");
      $(".nav-item").eq(0).addClass("active");
      $(".box-talks").css({"position": "absolute", "color": "#ffffff"});
      // console.log("beranda");
    } else if (tentang.offsetTop - 100 <= windo && team.offsetTop - 100 > windo) {
      $(".nav-item").eq(0).removeClass("active");
      $(".nav-item").eq(3).removeClass("active");
      $(".nav-item").eq(1).addClass("active");
      $(".box-talks").css({"position": "fixed", "color": "#3a3434"});
      // console.log("tentang");
    } else if (team.offsetTop - 100 <= windo && content.offsetTop - 100 > windo) {
      $(".nav-item").eq(1).removeClass("active");
      $(".nav-item").eq(4).removeClass("active");
      $(".nav-item").eq(3).addClass("active");
      // $(".box-talks").css({"position": "fixed", "color": "#3a3434", "display": "block"});
      $(".box-talks").css({"position": "fixed", "color": "#3a3434"});
      // console.log("kriteria");
    } else if (content.offsetTop - 100 <= windo && contact.offsetTop - 300 > windo) {
      $(".nav-item").eq(5).removeClass("active");
      $(".nav-item").eq(4).addClass("active");
      $(".nav-item").eq(3).removeClass("active");
      $(".box-talks").css({"position": "fixed", "color": "#3a3434", "display": "block"});
    } else if (contact.offsetTop - 300 <= windo) {
      $(".box-talks").css({"display": "none"});
    }

  })
  // -------- SCROLL SPY

  // ========== BTN SIGNIN
  $('#btnSignIn').on("click", function() {
    $('#link-log').addClass("active");
  })
  // ========== END BTN SIGNIN
</script>
<?= $this->endSection(); ?>