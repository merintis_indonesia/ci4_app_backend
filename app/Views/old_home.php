<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Merintis Indonesia</title>
  <!-- SEO -->
  <meta name="description" content="Merintis Indonesia - Merintis Indonesia adalah ekosistem kreatif muda/i daerah untuk saling terhubung, berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.">
  <meta name="keywords" content="Startup,UMKM,UKM,Daerah,Produksi,Merintis Indonesia,merintisindonesia,Madiun,Jawa Timur,Ide Bisnis,Technopreneur,Wirausaha,Startup Madiun,Digital,business ideas">
  <meta name="og:locale" content="id_ID">
  <meta name="og:type" content="website">
  <meta name="og:title" content="Merintis Indonesia">
  <meta name="og:description" content="Merintis Indonesia - Connect, Collaborate & Create">
  <meta name="og:url" content="https://merintisindonesia.com">
  <meta name="og:site_name" content="Merintis Indonesia">
  <meta name="og:image" content="https://merintisindonesia.com/assets/img/Picture15.png">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:title" content="Merintis Indonesia">
  <meta name="twitter:description" content="Yuk Jadi Pengusaha Muda Bersama Merintis Indonesia!">
  <!-- END SEO -->

  <!-- Favicons -->
	<link href="<?= base_url('favicon.png'); ?>" type="image/png" rel="icon">
  <link href="<?= base_url('apple-touch-icon.png'); ?>" rel="apple-touch-icon">

  <link rel="stylesheet" href= "<?= base_url('assets/vendors/bootstrap/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/fontawesome/css/all.min.css'); ?>">
  <script src="https://use.fontawesome.com/60a313a36b.js"></script>

  <link rel="stylesheet" href="<?= base_url('assets/css/home.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/swiper.min.css'); ?>">

  <style media="screen">
  /* =========== SWIPER CSS ======== */
  .swiper-container {
    width: 100%;
    padding-top: 50px;
    padding-bottom: 50px;
  }
  .swiper-slide {
    background-position: center;
    background-size: cover;
    width: 300px;
    height: 300px;
  }
  /* =========== END SWIPER CSS ======== */
  </style>

</head>
<body>
  <!--================ Header Menu Area start =================-->
  <header>
    <div class="mis-header">
      <div class="mis-header-logo">
        <a href="#">
          <img src="<?= base_url('assets/img/mis-white.png'); ?>" alt="logo-mis">
        </a>
      </div>
    </div>
    <!-- BATAS NAVBAR -->
    <nav class="navbar navbar-expand-sm navbar-dark bg-gold">
      <div class="mx-auto">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMis" aria-controls="navbarMis" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="navbarMis">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item">
            <a class="nav-link" href="#beranda">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#tentang">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#program">Program</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#team">Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#gallery">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://info-merintisindonesia.medium.com/">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" onclick="daftarMIS()">Pendaftaran</a>
            <!-- <a class="nav-link" href="javascript:void(0)" onclick="pengumumanMIS()">Pengumuman</a> -->
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact">Contact Us</a>
          </li>
          <li class="nav-item">
            <!-- Login/Logout -->
            <div id="link-log">
              <?= isset($_SESSION['id_akun']) && isset($_SESSION['token']) ? '<a class="nav-link" href="javascript:void(0)" onclick="logout()">Logout</a>' : '<a class="nav-link" data-toggle="modal" data-target="#modalLogin" href="javascript:void(0)">Login</a>'; ?>
            </div>
            <!-- End Login/Logout -->
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!--================ END Header Menu Area =================-->

  <!--================ FIXED ALERT =================-->
  <div class = 'fixed-alert'></div>
  <!--================ END FIXED ALERT =================-->

  <!--================ Modal Login Start =================-->
  <div class="modal zoom-in" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLoginLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <!-- ID MODAL -->
        <div id="modalFormUser">
          <!-- CONTENT MODAL -->
          <div class="modal-header">
            <h5 class="modal-title roboto-condensed h5-20" id="loginMI">Silahkan Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="container search-wrapper">
            <!--======== ALERT LOGIN -->
            <div id="alert-login"></div>
            <!--======== END ALERT LOGIN -->
            <form method="post" id="formLogin" class="custom-form">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Email" name="email">
                <div class="invalid-feedback">
                  Pesan error email
                </div>
              </div>
              <div class="form-group">
                <div class="lay-password-outline">
                  <input type="password" class="form-control password" placeholder="Password" name="passwd">
                  <a href="javascript:void(0)">
                    <i class="fa fa-eye-slash"></i>
                  </a>
                </div>
                <div id="err-passwd" class="invalid-feedback">
                  Pesan error password
                </div>
              </div>
            </form>
            <div class="form-group">
              <a id="btnDaftarAkun" class="button-kuning link-none border-0 mt-3 mx-3" href="daftar">Buat Akun</a>
              <button id="btnSubmitLogin" class="button border-0 mt-3">Login</button>
            </div>
            <span class="text-grey">Lupa password?</span> <a href="lupapass">Klik di sini</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--================ Modal Login End =================-->

  <!--================ Section Area =================-->
  <!-- ============ SECTION BERANDA -->
  <section id="beranda">
    <div class="container text-center">
        <h1 class="roboto-condensed text-white h1">Ayo wujudkan ide bisnismu bersama</h1>
        <h1 class="roboto-condensed text-white h1 mb-4">Merintis Indonesia Summit 2021!</h1>
        <button class="btn-hijau link-none" onclick="daftarMIS()">Daftar Sekarang</button>
        <!-- <h1 class="roboto-condensed text-white h1">Selamat kepada TOP 50 Bisnis Terpilih</h1>
        <h1 class="roboto-condensed text-white h1 mb-4">Merintis Indonesia Summit 2021!</h1>
        <button class="btn-hijau link-none" onclick="pengumumanMIS()">Cek Hasil TOP 50</button> -->
    </div>
  </section>

<!-- ============ SECTION TENTANG -->
  <section id="tentang">
    <div class="container text-center mt-48">
      <figure class="px-48">
          <img src="<?= base_url("/assets/img/logo-mis-color.png"); ?>" alt="logo-mis" class="img-100 mb-4">
          <figcaption>
            <p><b>Merintis Indonesia</b> adalah ekosistem kreatif muda/i daerah untuk saling terhubung,
              berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.</p>
          </figcaption>
      </figure>

      <div class="d-flex flex-wrap justify-content-center roboto-condensed">
        <div class="mx-3 pt-2">
          <h2 class="text-gold"><span class="countA">30</span>+</h2>
          <h5 class="text-green">Communities</h5>
        </div>
        <div class="mx-3 pt-2">
          <h2 class="text-gold"><span class="countA">10</span>+</h2>
          <h5 class="text-green">Cities</h5>
        </div>
        <div class="mx-3 pt-2">
          <h2 class="text-gold"><span class="countA">20</span>+</h2>
          <h5 class="text-green">Universities</h5>
        </div>
      </div>

      <div class="d-flex flex-wrap justify-content-around roboto-condensed mt-2">
        <div class="mt-1 mx-2 pt-2">
          <h1 class="text-gold"><span class="countB">795</span><span class="text-green text-32">/9 Talks</span></h2>
        </div>
        <div class="mt-1 mx-2 pt-2">
          <h1 class="text-gold"><span class="countB">38</span><span class="text-green text-32">/158 Intern</span></h2>
        </div>
      </div>

      <p class="roboto-condensed text-green text-18">OUR PARTNERS</p>
      <div class="d-flex flex-wrap justify-content-center">
        <img src="<?= base_url('/assets/img/Picture11.png'); ?>" alt="madiun-muda" class="img-50 mx-3">
        <img src="<?= base_url('/assets/img/Picture24.png'); ?>" alt="maesa-group" class="img-50 mx-3">
      </div>
      <div class="d-flex flex-wrap justify-content-center mt-3">
        <img src="<?= base_url('/assets/img/Picture12.png'); ?>" alt="ejsc" class="img-50 mx-3">
        <img src="<?= base_url('/assets/img/Picture23.png'); ?>" alt="madiun-pedia" class="img-50 mx-3">
        <img src="<?= base_url('/assets/img/Picture25.png'); ?>" alt="madiun-ae" class="img-50 mx-3">
        <img src="<?= base_url('/assets/img/Picture26.png'); ?>" alt="merakit" class="img-50 mx-3">
        <img src="<?= base_url('/assets/img/Picture22.png'); ?>" alt="rekom" class="img-25 mx-3 align-self-center">
      </div>
    </div>
  </section>

<!-- ============ SECTION PROGRAM -->
  <section id="program" class="mt-120">
    <div class="container">
      <h1 class="roboto-condensed text-gold text-center">PROGRAM</h1>
      <div class="d-flex flex-wrap justify-content-center mt-3">
        <img src="<?= base_url('/assets/img/Program1.png'); ?>" alt="pogram-mis-1" class="img-180 mx-4 my-2">
        <img src="<?= base_url('/assets/img/Program2.png'); ?>" alt="pogram-mis-2" class="img-180 mx-4 my-2">
        <img src="<?= base_url('/assets/img/Program3.png'); ?>" alt="pogram-mis-3" class="img-180 mx-4 my-2">
        <img src="<?= base_url('/assets/img/Program4.png'); ?>" alt="pogram-mis-4" class="img-180 mx-4 my-2">
        <img src="<?= base_url('/assets/img/Program5.png'); ?>" alt="pogram-mis-5" class="img-180 mx-4 my-2">
      </div>
      <!-- SEMINAR -->
      <div class="d-flex flex-wrap justify-content-center mt-3">
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar1.png'); ?>" alt="seminar-1" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CEAl74CpIYn/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #01</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar2.png'); ?>" alt="seminar-2" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CEti_ODgJOG/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #02</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar3.png'); ?>" alt="seminar-3" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CFEEBynAXlM/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #03</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar4.png'); ?>" alt="seminar-4" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CFT0YcnAm_W/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #04</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar5.png'); ?>" alt="seminar-5" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CFoRoRNAXkG/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #05</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar6.png'); ?>" alt="seminar-6" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CGKFDbYjoCe/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #06</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar7.png'); ?>" alt="seminar-7" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CHKctQMjZ89/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #07</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar8.png'); ?>" alt="seminar-8" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CHeYL67Dc0M/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #08</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar9.png'); ?>" alt="seminar-8" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.instagram.com/p/CIDfKCBj1TC/" target="_blank" class="btn-hijau link-none text-white">MIS TALK #09</a>
          </div>
        </div>

        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Seminar10.png'); ?>" alt="seminar-8" class="img-180 mx-4 my-2">
          <div class="card-body text-center">
            <a href="https://www.youtube.com/watch?v=5MirYjKHzAM" target="_blank" class="btn-hijau link-none text-white">MIS TALK #10</a>
          </div>
        </div>
      </div>

    </div>
  </section>

<!-- ============ SECTION TEAM -->
  <section id="team" class="mt-120">
    <div class="container roboto-condensed text-center">
      <h1 class="text-green">FOUNDER</h1>
      <h5 class="text-gold">MERINTIS INDONESIA</h5>
      <div class="d-flex flex-wrap justify-content-center mt-3">
        <img src="<?= base_url('/assets/img/Founder1.png'); ?>" alt="Founder-1" class="img-295 mx-4 my-1">
        <img src="<?= base_url('/assets/img/Founder2.png'); ?>" alt="Founder-2" class="img-295 mx-4 my-1">
        <img src="<?= base_url('/assets/img/Founder3.png'); ?>" alt="Founder-3" class="img-295 mx-4 my-1">
        <img src="<?= base_url('/assets/img/Founder4.png'); ?>" alt="Founder-4" class="img-295 mx-4 my-1">
      </div>

      <!-- VIP TEAM -->
      <h1 class="text-green mt-64">VIRTUAL INTERN</h1>
      <div class="d-flex flex-wrap justify-content-center mt-3">
        <!-- BARIS 1 -->
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip1.png'); ?>" alt="Vip1" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Miftahul Fauza R</span>
            <span class="block text-gold text-12 mr-2">Universitas PGRI Madiun</span>
            <span class="block roboto text-grey text-10 mr-2">Teknik Informatika</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip2.png'); ?>" alt="Vip2" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Jahfal Uno S L</span>
            <span class="block text-gold text-12 mr-2">Universitas Gunadarma</span>
            <span class="block roboto text-grey text-10 mr-2">Teknik Informatika</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip3.png'); ?>" alt="Vip3" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Jeffrens Tanadi</span>
            <span class="block text-gold text-12 ml-4">Binus University</span>
            <span class="block roboto text-grey text-10 ml-4">Business Information Technology</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip4.png'); ?>" alt="Vip4" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Pedro Ozora</span>
            <span class="block text-gold text-12 ml-4">Institut Teknologi Del</span>
            <span class="block roboto text-grey text-10 ml-4">Teknologi Komputer</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip5.png'); ?>" alt="Vip5" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Audrey Dian F</span>
            <span class="block text-gold text-12 ml-4">Institut Teknologi Bandung</span>
            <span class="block roboto text-grey text-10 ml-4">Matematika</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip6.png'); ?>" alt="Vip6" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Mutia Mahhal</span>
            <span class="block text-gold text-12 ml-4">Universitas Andalas</span>
            <span class="block roboto text-grey text-10 ml-4">Ilmu Komunikasi</span>
          </div>
        </div>
        <div class="card border-none mr-1">
          <img src="<?= base_url('/assets/img/Vip7.png'); ?>" alt="Vip7" class="img-180 my-2">
          <div class="text-left">
            <span class="block text-green text-18 ml-4">Firdausyi Nuzulah</span>
            <span class="block text-gold text-12 ml-4">Politeknik Negeri Jakarta</span>
            <span class="block roboto text-grey text-10 ml-4">Manajemen Keuangan</span>
          </div>
        </div>

        <!-- BARIS 2 -->
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip8.png'); ?>" alt="Vip8" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Dedi</span>
            <span class="block text-gold text-12 mr-2">Universitas Padjajaran</span>
            <span class="block roboto text-grey text-10 mr-2">Sastra Indonesia</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip9.png'); ?>" alt="Vip9" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Dian Rahmadani L</span>
            <span class="block text-gold text-10 mr-2">Universitas Negeri Surabaya</span>
            <span class="block roboto text-grey text-10 mr-2">Ilmu Komunikasi</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip10.png'); ?>" alt="Vip10" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Cindy Marilyn C</span>
            <span class="block text-gold text-10 mr-2">Universitas Darma Persada</span>
            <span class="block roboto text-grey text-10 mr-2">Sastra Jepang</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip11.png'); ?>" alt="Vip11" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Eldiastari Putri A</span>
            <span class="block text-gold text-12 mr-2">Universitas Gunadarma</span>
            <span class="block roboto text-grey text-10 mr-2">Ilmu Komunikasi</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip12.png'); ?>" alt="Vip12" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Adhini Bintang P</span>
            <span class="block text-gold text-12 mr-2">Universitas Diponegoro</span>
            <span class="block roboto text-grey text-10 mr-2">Administrasi Bisnis</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip13.png'); ?>" alt="Vip13" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Chenny Chang</span>
            <span class="block text-gold text-8 mr-2">Institut Pariwisata dan Bisnis Internasional</span>
            <span class="block roboto text-grey text-10 mr-2">Hospitality Management</span>
          </div>
        </div>
        <div class="card border-none">
          <img src="<?= base_url('/assets/img/Vip14.png'); ?>" alt="Vip14" class="img-180 my-2">
          <div class="text-right mr-4">
            <span class="block text-green text-18 mr-2">Al-Hanaan</span>
            <span class="block text-gold text-12 mr-2">Universitas Terbuka</span>
            <span class="block roboto text-grey text-10 mr-2">Ekonomi Pembangunan</span>
          </div>
        </div>

        <!-- DISEMBUNYIKAN -->
          <!-- BARIS 3 -->
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip15.png'); ?>" alt="Vip15" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Aldo Putra</span>
              <span class="block text-gold text-12 ml-4">Universitas Gadjah Mada</span>
              <span class="block roboto text-grey text-10 ml-4">Teknik Fisika</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip16.png'); ?>" alt="Vip16" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Leonardo Anthony</span>
              <span class="block text-gold text-12 ml-4">Universitas Gadjah Mada</span>
              <span class="block roboto text-grey text-10 ml-4">Teknik Kimia</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip17.png'); ?>" alt="Vip17" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Dwi Khoiriyah</span>
              <span class="block text-gold text-12 ml-4">Politeknik Negeri Jakarta</span>
              <span class="block roboto text-grey text-10 ml-4">Administrasi Bisnis Terapan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip18.png'); ?>" alt="Vip18" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Raihan Ariq</span>
              <span class="block text-gold text-12 ml-4">Binus University</span>
              <span class="block roboto text-grey text-10 ml-4">IT dan Statistika</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip19.png'); ?>" alt="Vip19" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Novitriana Gadis</span>
              <span class="block text-gold text-12 ml-4">Telkom University</span>
              <span class="block roboto text-grey text-10 ml-4">Sistem Informasi</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip20.png'); ?>" alt="Vip20" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Asna Af'idatul</span>
              <span class="block text-gold text-12 ml-4">Universitas Brawijaya</span>
              <span class="block roboto text-grey text-10 ml-4">Ilmu Administrasi Bisnis</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip21.png'); ?>" alt="Vip21" class="img-180 mx-1 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Yuwinda Meysella</span>
              <span class="block text-gold text-12 mr-2">Universitas Negeri Malang</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>

          <!-- BARIS 4 -->
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip22.png'); ?>" alt="Vip22" class="img-180 mx-1 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Mutiara Suci R</span>
              <span class="block text-gold text-12 mr-2">IKOPIN</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip23.png'); ?>" alt="Vip23" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Jovita Aurelia</span>
              <span class="block text-gold text-10 mr-2">Universitas Katolik Atma Jaya</span>
              <span class="block roboto text-grey text-10 mr-2">Teknologi Pangan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip24.png'); ?>" alt="Vip24" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Bagus Pamungkas</span>
              <span class="block text-gold text-12 mr-2">UNMUH Prof Dr Hamka</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip25.png'); ?>" alt="Vip25" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Winny Maisyarah</span>
              <span class="block text-gold text-12 mr-2">Universitas Sumatera Utara</span>
              <span class="block roboto text-grey text-10 mr-2">Teknik Elektro</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip26.png'); ?>" alt="Vip26" class="img-180 mx-1 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Reni Ridayanti</span>
              <span class="block text-gold text-12 mr-2">Universitas Negeri Malang</span>
              <span class="block roboto text-grey text-10 mr-2">Ekonomi Pembangunan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip27.png'); ?>" alt="Vip27" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Maasa Sunreza M</span>
              <span class="block text-gold text-12 ml-4">Universitas Airlangga</span>
              <span class="block roboto text-grey text-10 ml-4">Fakultas Kedokteran</span>
            </div>
          </div>
          <!-- BARIS 5 -->
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip28.png'); ?>" alt="Vip28" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Asmawi Anwar</span>
              <span class="block text-gold text-12 ml-4">Universitas PGRI Madiun</span>
              <span class="block roboto text-grey text-10 ml-4">Pendidikan Guru Sekolah Dasar</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip29.png'); ?>" alt="Vip29" class="img-180 mx-1 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Rr. Nektara Titan</span>
              <span class="block text-gold text-12 ml-4">Universitas Jember</span>
              <span class="block roboto text-grey text-10 ml-4">Pendidikan Dokter Gigi</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip30.png'); ?>" alt="Vip30" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Emi Yanti P</span>
              <span class="block text-gold text-12 mr-2">Universitas Diponegoro</span>
              <span class="block roboto text-grey text-10 mr-2">Manajemen</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip31.png'); ?>" alt="Vip31" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Jessica Ananda</span>
              <span class="block text-gold text-12 mr-2">President University</span>
              <span class="block roboto text-grey text-10 mr-2">Teknik Industri</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip32.png'); ?>" alt="Vip32" class="img-180 mx-1 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Mujahid Najib</span>
              <span class="block text-gold text-12 mr-2">Universitas Indonesia</span>
              <span class="block roboto text-grey text-10 mr-2">Geofisika</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip33.png'); ?>" alt="Vip33" class="img-180 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Haryadi Bagus</span>
              <span class="block text-gold text-12 ml-4">Universitas Brawijaya</span>
              <span class="block roboto text-grey text-10 ml-4">Peternakan</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip34.png'); ?>" alt="Vip34" class="img-180 mx-1 my-2">
            <div class="text-left">
              <span class="block text-green text-18 ml-4">Syachfara N</span>
              <span class="block text-gold text-12 ml-4">Universitas Bina Nusantara</span>
              <span class="block roboto text-grey text-10 ml-4">Public Relations</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip35.png'); ?>" alt="Vip35" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Diva Zulfania T</span>
              <span class="block text-gold text-12 mr-2">Universitas Negeri Malang</span>
              <span class="block roboto text-grey text-10 mr-2">Teknik Mesin</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip36.png'); ?>" alt="Vip36" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Syafira Nur Alifah</span>
              <span class="block text-gold text-12 mr-2">Universitas Padjajaran</span>
              <span class="block roboto text-grey text-10 mr-2">Hubungan Internasional</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip">
            <img src="<?= base_url('/assets/img/Vip37.png'); ?>" alt="Vip37" class="img-180 my-2">
            <div class="text-right mr-4">
              <span class="block text-green text-18 mr-2">Anggraeni Dwi A</span>
              <span class="block text-gold text-12 mr-2">Griffith University Australia</span>
              <span class="block roboto text-grey text-10 mr-2">Hubungan Internasional</span>
            </div>
          </div>
        <!-- END DISEMBUNYIKAN -->

    </div>
    <!-- BUTTON_SELENGKAPNYA -->
    <button type="button" class="button-kuning border-none roboto mt-4" data-toggle="collapse" data-target=".hide-vip" id="btnMore">Selengkapnya</button>
  </section>

  <!-- ============ SECTION GALLERY -->
  <section id="gallery">
    <div class="container">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <div class="swiper-slide" style="background-image:url(<?= base_url('/assets/img/galeri1.png'); ?>)"></div>
          <div class="swiper-slide" style="background-image:url(<?= base_url('/assets/img/galeri2.png'); ?>)"></div>
          <div class="swiper-slide" style="background-image:url(<?= base_url('/assets/img/galeri3.png'); ?>)"></div>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
      </div>
    </div>
  </section>

  <!-- ============ SECTION CONTACT US -->
  <section id="contact" class="text-center">
    <div class="mis-header-logo-100">
        <img src="<?= base_url('assets/img/mis-white.png'); ?>" alt="logo-mis">
    </div>
    <h3 class="roboto-condensed text-white mt-2">MERINTIS INDONESIA</h3>
    <h3 class="roboto-condensed text-white mb-4">BERSAMA PENGUSAHA MUDA</h3>
    <span class="btn-hijau px-4">Contact Us</span>
    <div class="d-flex justify-content-center flex-nowrap mt-4">
      <span class="sosmed">
        <a class="overlay" href="https://wa.me/+6285785036770" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed1.svg'); ?>" alt="WA">
      </span>
      <span class="sosmed">
        <a class="overlay" href="https://www.facebook.com/merintisindonesia/" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed2.svg'); ?>" alt="fb">
      </span>
      <span class="sosmed">
        <a class="overlay" href="https://open.spotify.com/show/2v1GGLrQU076JQ9LZTLi52?si=nf_wLH8JTUOSOcStfSdZ-w" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed3.svg'); ?>" alt="spotify">
      </span>
    </div>
    <div class="d-flex justify-content-center flex-nowrap mt-2">
      <span class="sosmed">
        <a class="overlay" href="https://www.instagram.com/merintis.indonesia/" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed4.svg'); ?>" alt="IG">
      </span>
      <span class="sosmed">
        <a class="overlay" href="https://twitter.com/MerintisID?s=08" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed5.svg'); ?>" alt="twitter">
      </span>
      <span class="sosmed">
        <a class="overlay" href="https://vt.tiktok.com/ZStJL8Gp/" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed6.svg'); ?>" alt="tiktok">
      </span>
    </div>
    <div class="d-flex justify-content-center flex-nowrap mt-2">
      <span class="sosmed">
        <a class="overlay" href="mailto:info.merintisindonesia@gmail.com" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed7.svg'); ?>" alt="email">
      </span>
      <span class="sosmed">
        <a class="overlay" href="https://www.linkedin.com/company/merintis-indonesia" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed8.svg'); ?>" alt="linkedin">
      </span>
      <span class="sosmed">
        <a class="overlay" href="https://info-merintisindonesia.medium.com/" target="_blank"></a>
        <img src="<?= base_url('assets/img/sosmed9.svg'); ?>" alt="link">
      </span>
    </div>
  </section>
  <!--================ END Section Area =================-->

  <!--================ Footer Area =================-->
  <footer class="roboto py-2">
    <div class="container text-center">
      <span class="text-grey">Copyright &copy; <span id="yearNow">tahun</span> All rights reserved</span>
    </div>
  </footer>
  <!--================ END Footer Area =================-->

  <script src="<?= base_url ('assets/vendors/jquery/jquery-3.2.1.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/vendors/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/js/swiper.min.js'); ?>"></script>
  <script>
    // Tambahan Script

    // SMOOTH SCROLL
    $(document).ready(function() {
      $("a").on("click", function(event) {
        if (this.hash !== "") {
          event.preventDefault();
          let hash = this.hash;

          $("html, body").animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
            window.location.hash = hash;
          });
        }
      });
    });

    // ----------- CEK JIKA DARI HALAMAN DAFTAR -----------
    $( document ).ready(function() {
      let params = (new URL(document.location)).searchParams;
      let stat = parseInt(params.get('stat'));
      if (stat) {
        $("#modalLogin").modal('toggle');
      }
    });
    // ----------- END CEK JIKA DARI HALAMAN DAFTAR -----------

    // SHOW NAVBAR FIXED
    window.onscroll = changeNav;
    function changeNav() {
      let navbar = $('nav');
      if (window.pageYOffset > 90) {
        navbar.addClass('navbar-fixed')
      } else {
        navbar.removeClass('navbar-fixed');
      }
    }

    // GET YEAR NOW
    let date = new Date();
    let now = date.getFullYear();
    document.getElementById("yearNow").innerHTML = now;

    // COUNT UP ANIMATION
    $(document).ready(function() {
      countUp(".countA");
      countUp(".countB");
      function countUp(target) {
        let a = 0;
        $(window).scroll(function() {

          let oTop = $(target).offset().top - window.innerHeight;
          if (a == 0 && $(window).scrollTop() > oTop) {
            $(target).each(function () {
              let $this = $(this);
              jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
                duration: 1250,
                easing: 'swing',
                step: function () {
                  $this.text(Math.ceil(this.Counter));
                }
              });
            });
            a = 1;
          }
        });
      }
    });

    // =========== Show Login/Logout ============
    function swBtnLogin() {
      let login = `<a class="nav-link" data-toggle="modal" data-target="#modalLogin" href="javascript:void(0)">Login</a>`
      $('#link-log').html(login);
    }

    function swBtnLogout() {
      let logout = `<a class="nav-link" href="javascript:void(0)" onclick="logout()">Logout</a>`
      $('#link-log').html(logout);
    }
    // =========== END Show Login/Logout ============

    // ==================== LOGOUT ===================
    function logout() {
      //console.log("Berhasil logout");
      // --------------- LOGOUT -------------------
      $(this).on("click", function() {
        // ------ MENGHAPUS SESSION DI DATABASE
        // let session_id = sessionStorage.getItem("session_id");
        // let session_token = sessionStorage.getItem("session_token");
        $('#link-log').addClass("active");
        let session_id = "<?= isset($_SESSION['id_akun']) ? $_SESSION['id_akun'] : ''; ?>";
        let session_token = "<?= isset($_SESSION['token']) ? $_SESSION['token'] : ''; ?>";
        $.ajax({
          type: "post",
          url: "<?= base_url('/akun/hapussession'); ?>",
          data: {"id_akun": session_id},
          success: function() {
            window.location.replace("<?= base_url(); ?>");
          }
        })
        // ------ END MENGHAPUS SESSION DI DATABASE

        // ------ MENGHAPUS ITEM DI SESSION STORAGE
          // sessionStorage.removeItem("session_id");
          // sessionStorage.removeItem("session_token");
        // ---- UBAH MENJADI BUTTON LOGIN
        // let login = `<a class="nav-link" data-toggle="modal" data-target="#modalLogin" href="javascript:void(0)">Login</a>`
        // $("#link-log").html(login);
      })
    }
    // ==================== END LOGOUT ===============

    //------------ SHOW PASSWORD ---------------
    $(document).ready(function() {
      $(".lay-password-outline a").on('click', function(event) {
        event.preventDefault();
        if($('.lay-password-outline input').attr("type") == "text"){
          $('.lay-password-outline input').attr('type', 'password');
          $('.lay-password-outline i').addClass( "fa-eye-slash" );
          $('.lay-password-outline i').removeClass( "fa-eye" );
        }else if($('.lay-password-outline input').attr("type") == "password"){
          $('.lay-password-outline input').attr('type', 'text');
          $('.lay-password-outline i').removeClass( "fa-eye-slash" );
          $('.lay-password-outline i').addClass( "fa-eye" );
        }
      });
    });
    //------------ END-SHOW PASSWORD ---------------

    // ----------- FUNCTION PASSWORD SHOW INVALID MSG -----------------
    function passwdErr() {
      if($(".lay-password-outline").hasClass("password-invalid"))
      {
        $("#err-passwd").show();
      } else {
        $("#err-passwd").hide();
      }
    }
    // ----------- END FUNCTION PASSWORD SHOW INVALID MSG -----------------

    // ----------- ALERT BELUM TERDAFTAR -----------------
    function alertLoginBerhasil(pesan) {
      let alBerhasil = `
      <div class="alert alert-success alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
      $('#alert-login').html(alBerhasil)
    }

    function alertLoginGagal(pesan) {
      let alGagal = `
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
      $('#alert-login').html(alGagal)
    }
    // ----------- END ALERT BELUM TERDAFTAR -----------------

    // ----------- CEK JIKA ADA SESSION
    // if (sessionStorage.getItem("session_id") === null && sessionStorage.getItem("token") === null) {
    //   // ---JIKA TIDAK ADA SESSION
    //   swBtnLogin();
    // } else {
    //   // ---JIKA ADA SESSION
    //   swBtnLogout();
    //   // ----------------- AMBIL DATA
    //   $(document).ready(function() {
    //     $.ajax({
    //       type: "post",
    //       url: "<?= base_url('/akun/ambillogin'); ?>",
    //       data: {"session_id": sessionStorage.getItem("session_id")},
    //       error: function(response) {
    //         //console.log(response.responseJSON.message);
    //         alFixedGagal(response.responseJSON.message)
    //       }
    //     })
    //   })
    //   // ----------------- END AMBIL DATA
    // }
    // ----------- END CEK JIKA ADA SESSION

    // ------------------ FIXED ALERT
    function alFixedBerhasil(pesan) {
      let alBerhasil = `
      <div class="alert alert-success alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
      $('.fixed-alert').html(alBerhasil)
      $('.fixed-alert').html(alBerhasil).animate({
        right: '12px'
      })

      $('.fixed-alert .close').on("click", function() {
        $('.fixed-alert').animate({
          right: '-100px'
        });
        $('.fixed-alert .alert').removeClass('.show');
      })
    }

    function alFixedGagal(pesan) {
      let alGagal = `
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
      $('.fixed-alert').html(alGagal).animate({
        right: '12px'
      })

      $('.fixed-alert .close').on("click", function() {
        $('.fixed-alert').animate({
          right: '-100px'
        });
        $('.fixed-alert .alert').removeClass('.show');
      })
    }
    // -------------------------- END FIXED ALERT

    // ---------------- KIRIM DATA LOGIN ------
    $('#btnSubmitLogin').on('click', function() {
      $.ajax({
        type: "post",
        url: "<?= base_url('/akun/loginakun'); ?>",
        data: $('#formLogin').serialize(),
        beforeSend: function() {
          $("#btnSubmitLogin").attr('disabled', true);
          $("#btnSubmitLogin").addClass('wait');
        },
        success: function(data) {
          // console.log(data);
          if (data.success) {
            // console.log(data.data);
            // -------ALERT BERHASIL
            alertLoginBerhasil(data.message);
            //------ENABLED BUTTON
            $("#btnSubmitLogin").attr('disabled', false);
            $("#btnSubmitLogin").removeClass('wait');
            // ---- CLEAR ERROR TEXT
            $(".form-control").eq(0).removeClass("is-invalid");
            $(".lay-password-outline").removeClass("password-invalid");
            passwdErr();
            // ----- UBAH BUTTON
            swBtnLogout();
            // ----- SET SESSION
              // sessionStorage.setItem("session_id", data.session_id);
              // sessionStorage.setItem("session_token", data.session_token);
            // ---- BERSIHKAN FORM
            $( '#formLogin' ).each(function(){
              this.reset();
            });
            // REDIRECT KE HALAMAN AWAL
            window.location.replace("<?= base_url(); ?>");
          } else {
            //------ENABLED BUTTON
            $("#btnSubmitLogin").attr('disabled', false);
            $("#btnSubmitLogin").removeClass('wait');
            // ----------- AKUN BELUM TERDAFTAR/PASS SALAH
            if (data.message) {
              alertLoginGagal(data.message);
              // ---- CLEAR ERROR TEXT
              $(".form-control").eq(0).removeClass("is-invalid");
              $(".lay-password-outline").removeClass("password-invalid");
            }
            // ----------- ERROR EMAIL
            if (data.error.email) {
              $(".form-control").eq(0).addClass("is-invalid");
              $(".invalid-feedback").eq(0).html(data.error.email);
            } else {
              $(".form-control").eq(0).removeClass("is-invalid");
            }
            // ----------- ERROR PASSWORD
            if (data.error.password) {
              $(".lay-password-outline").addClass("password-invalid");
              $(".invalid-feedback").eq(1).html(data.error.password);
              passwdErr();
            } else {
              $(".lay-password-outline").removeClass("password-invalid");
              passwdErr();
            }
          }
        },
        error: function(response) {
          // console.log(response.responseJSON.message);
          if (response.responseJSON.message) {
            alertLoginGagal(response.responseJSON.message)
          }
          // ---- CLEAR ERROR TEXT
          $(".form-control").eq(0).removeClass("is-invalid");
          $(".lay-password-outline").removeClass("password-invalid");
          passwdErr();
          //------ENABLED BUTTON
          $("#btnSubmitLogin").attr('disabled', false);
          $("#btnSubmitLogin").removeClass('wait');
        }
      })
    })
    // ---------------- END KIRIM DATA LOGIN ------

    // ------------------ BTN COLLAPSE VIP ----------------
    $('.hide-vip').on('show.bs.collapse', function () {
      $('#btnMore').html('Sedikit')
    })
    $('.hide-vip').on('hide.bs.collapse', function () {
      $('#btnMore').html('Selengkapnya')
    })
    // ------------------ END BTN COLLAPSE VIP ----------------

    // -------- SCROLL SPY
    let beranda = document.querySelector("#beranda");
    let tentang = document.querySelector("#tentang");
    let program = document.querySelector("#program");
    let team = document.querySelector("#team");
    let gallery = document.querySelector("#gallery");
    let contact = document.querySelector("#contact");

    window.addEventListener("scroll", () => {
      var windo = window.pageYOffset;
      if (beranda.offsetTop <= windo && tentang.offsetTop - 100 > windo) {
        $(".nav-item").eq(1).removeClass("active");
        $(".nav-item").eq(0).addClass("active");
        // console.log("beranda");
      }
      else if (tentang.offsetTop - 100 <= windo && program.offsetTop - 100 > windo) {
        $(".nav-item").eq(0).removeClass("active");
        $(".nav-item").eq(2).removeClass("active");
        $(".nav-item").eq(1).addClass("active");
        // console.log("tentang");
      }
      else if (program.offsetTop - 100 <= windo && team.offsetTop - 100 > windo) {
        $(".nav-item").eq(1).removeClass("active");
        $(".nav-item").eq(3).removeClass("active");
        $(".nav-item").eq(2).addClass("active");
        // console.log("terms");
      }
      else if (team.offsetTop - 100 <= windo && gallery.offsetTop - 100 > windo) {
        $(".nav-item").eq(2).removeClass("active");
        $(".nav-item").eq(4).removeClass("active");
        $(".nav-item").eq(3).addClass("active");
        // console.log("kriteria");
      }
      else if (gallery.offsetTop - 100 <= windo && contact.offsetTop - 100 > windo) {
        $(".nav-item").eq(3).removeClass("active");
        $(".nav-item").eq(7).removeClass("active");
        $(".nav-item").eq(4).addClass("active");
        // console.log("kriteria");
      }
      else if (contact.offsetTop - 100 <= windo) {
        $(".nav-item").eq(4).removeClass("active");
        $(".nav-item").eq(7).addClass("active");
        // console.log("seleksi");
      }
    })
    // -------- SCROLL SPY

    // ------------------ SWIPER JS ---------------------
    let swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      loop: true,
      slidesPerView: 'auto',
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });
    // ------------------ END SWIPER JS ---------------------

    function daftarMIS() {
      let cekData = "<?= isset($_SESSION['token']) ? $_SESSION['token'] : ''; ?>";
      if (!cekData) {
        alFixedGagal("Anda belum login")
      } else {
        window.location.href = "<?= base_url('mis#registration') ?>"
      }
    }

    function pengumumanMIS() {
      // if (sessionStorage.getItem("session_id") === null && sessionStorage.getItem("token") === null) {
      let cekData = "<?= isset($_SESSION['token']) ? $_SESSION['token'] : ''; ?>";
      if (!cekData) {
        alFixedGagal("Anda belum login")
      } else {
        window.location.href = "<?= base_url('mis#pengumuman') ?>"
      }
    }

  </script>
</body>
</html>
