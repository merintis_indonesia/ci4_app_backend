<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Merintis Indonesia Summit 2021</title>
  <!-- SEO -->
  <meta name="description" content="Merintis Indonesia - Connect, Collaborate & Create">
  <meta name="keywords" content="Startup,UMKM,UKM,Daerah,Produksi,Merintis Indonesia,merintisindonesia,Madiun,Jawa Timur,Ide Bisnis,Technopreneur,Wirausaha,Startup Madiun,Digital,business ideas">
  <meta name="og:locale" content="id_ID">
  <meta name="og:type" content="website">
  <meta name="og:title" content="Merintis Indonesia">
  <meta name="og:description" content="Connect, Collaborate & Create">
  <meta name="og:url" content="https://merintisindonesia.com">
  <meta name="og:site_name" content="Merintis Indonesia">
  <meta name="og:image" content="https://merintisindonesia.com/assets/img/Picture15.png">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:title" content="Merintis Indonesia">
  <meta name="twitter:description" content="Yuk Jadi Pengusaha Muda Bersama Merintis Indonesia!">
  <!-- END SEO -->

  <!-- Favicons -->
	<link href="<?= base_url('favicon.png'); ?>" type="image/png" rel="icon">
  <link href="<?= base_url('apple-touch-icon.png'); ?>" rel="apple-touch-icon">

  <link rel="stylesheet" href= "<?= base_url('assets/vendors/bootstrap/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/fontawesome/css/all.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/themify-icons/themify-icons.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/linericon/style.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/owl-carousel/owl.theme.default.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/owl-carousel/owl.carousel.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/flat-icon/font/flaticon.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/nice-select/nice-select.css'); ?>">
  <script src="https://use.fontawesome.com/60a313a36b.js"></script>

  <link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/mis.css'); ?>">
  <style>
    .kuning {
      color: #f9aa32;
    }
    .red {
      color: red;
    }
    .hijau {
      color: #45b54b;
    }
    .hitam {
      color: #000000;
    }
    .size-100px {
      font-size: 100px;
    }
    .size-10px {
      font-size: 12px;
    }
    .size-80px {
      font-size: 75px;
    }
    .size-110px {
      font-size: 70px;
    }
    .size-150px {
      font-size: 90px;
    }
    .size-30px {
      font-size: 30px;
    }
    .size-25px {
      font-size: 25px;
    }
    .size-60px {
      font-size: 20px;
    }
    .size-120px {
      font-size: 20px;
    }
    .size-70px {
      font-size: 65px;
    }
    .size-20px {
      font-size: 20px;
    }
    .size-120px {
      font-size: 50px;
    }
    .small-red {
      color: #e8472e;
      font-size: 12px;
    }
    .logo {
      max-width: 100px;
    }
    .sponsor img {
      max-height: 50px;
      border-radius: 7px;
    }
    .fluid {
      max-width: 165px;
      max-height: 165px;
    }
    .fluidd {
      max-width: 200px;
      max-height: 300px;
    }
    .img-small {
      max-width: 100px;
      padding-right: 20px;
    }
    .img-smalll {
      max-width: 80px;
      padding-right: 20px;
    }
    .img-big {
      padding-right: 20px;
    }
    .img-bigg {
      max-width: 100px;
      max-height: 100px;
    }
    .pd-top {
      padding-top: 20px;
    }
    .p {
      margin-left:-120px;
    }
    .h {
      margin-top: -10px;
      margin-left:-120px;
    }
    .i {
      margin-top:-100px;
      margin-left: 780px;
    }
    .padding-top-small {
      padding-top: 15px;
    }
    .has-error input, select{
      border-width: 1px;
      border-color: red;
      border-style: solid;
    }
    @media (min-height:1000px) {
      .padding-top-content {
        padding-top: 100px;
      }
    }
    @media (max-height:999px) {
      .padding-top-content {
        padding-top: 80px;
      }
    }
    table.table-custom {
      /* table-layout: fixed; */
      width: 100%;
    }
    table.table-custom td {
      padding: 10px;
      /* word-wrap: break-word; */
    }
    /* table.table-contact {
      table-layout: fixed;
      width: 100%;
    }
    table.table-contact td {
      padding: 10px;
      word-wrap: break-word;
    } */
    .text-center {
      text-align: center;
    }
    .text-justify {
      text-align: justify;
    }
    .h10 {
      margin-left: 123px;
    }
    .h11 {
      margin-left: -60px;
    }
    .h12 {
      margin-left: -410px;

    }
    .h13 {
      margin-left: 410px;
    }
    .h14 {
      margin-top: 20px;
    }
  </style>
</head>
<body class="bg-shape">
  <!--================ Header Menu Area start =================-->
 <header class="header_area">
   <div class="main_menu">
     <nav class="navbar navbar-expand-lg navbar-light">
       <div class="container box_1620 ">
         <div class="brand-login">
           <a class="navbar-brand logo_h" href="<?= base_url(); ?>"><img class="img-bigg" src="<?= base_url('/assets/img/Picture1.png'); ?>" alt="logo-mis"></a>
         </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
         </button>

         <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
           <ul class="nav navbar-nav menu_nav justify-content-end align-middle">
             <li class="nav-item"><a class="nav-link" href="#beranda">Beranda</a></li>
             <li class="nav-item"><a class="nav-link" href="#tentang">Tentang</a></li>
             <li class="nav-item"><a class="nav-link" href="#terms">Syarat Ketentuan</a></li>
             <li class="nav-item"><a class="nav-link" href="#kriteria">Kriteria Seleksi</a></li>
             <li class="nav-item"><a class="nav-link" href="#seleksi">Timeline</a></li>
             <!-- <li class="nav-item"><a class="nav-link" href="#pengumuman">Pengumuman</a></li> -->
             <li class="nav-item"><a class="nav-link" href="#registration">Daftar</a></li>
             <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
           </ul>
         </div>
       </div>
     </nav>
   </div>
 </header>
 <!--================Header Menu Area =================-->

  <!--================ FIXED ALERT =================-->
  <div class = 'fixed-alert'></div>
  <!--================ END FIXED ALERT =================-->

  <!--================Hero Banner Area Start =================-->
  <section class="section-margin generic-margin padding-top-content order-2 order-lg-1" id="beranda">
    <div class="container mt-5">
      <div class="section-intro text-center pb-60px">
        <br>
        <img style="max-width: 100%;" class="" src="<?= base_url('assets/img/Picture14.png'); ?>" alt="">
        <h1 class="hijau size-110px">MERINTIS INDONESIA</h1>
        <h1 style=" line-height: 75px" class="hijau size-150px  ">SUMMIT 2021</h1>
        <br>
        <p class="font-bold hitam ">YUK JADI PENGUSAHA MUDA BERSAMA MERINTIS INDONESIA!</p>
        <br>
        <br>
        <br>
        <img class="" src="<?= base_url('assets/img/Picture15.png'); ?>" alt="">
      </div>
    </div>
  </section>
  <section class="section-margin generic-margin padding-top-content" id="tentang">
    <div class="container">
      <div class="section-intro text-lg-left pb-60px">
        <h1 class="hijau text-lg-left">TENTANG</h1>
        <h1 class="kuning text-lg-left">MERINTIS INDONESIA SUMMIT 2021</h1>
        <p class="hitam text-justify">Merintis Indonesia Summit merupakan program kompetisi kewirausahaan yang memberikan
          kesempatan kepada muda/i Indonesia yang berani membangun ide-ide
          bisnis daerah yang solutif, inovatif dan aplikatif untuk:</p>
        </div>
      </div>

      <div class="container">
        <div class="section-intro pb-10 px-3">
          <div class="row hitam text-justify">
            <div class="col-sm-6">
              <ul class="tanda">
                <li class="py-1">Memperoleh hibah modal usaha dengan total Rp50.000.000, pendampingan, serta akses edukasi bisnis.</li>
                <li class="py-1">Mewujudkan dan mengembang-kan ide-ide bisnis muda/i lokal yang menjawab permasalahan yang ada di daerah sekitar.</li>
              </ul>
            </div>
            <div class="col-sm-6">
              <ul class="tanda">
                <li class="py-1">Mematangkan konsep dan mendapat sesi mentoring bagi 30 finalis ide bisnis terbaik dalam tahap inkubasi online selama 3 hari.</li>
                <li class="py-1">Bergabung dalam jaringan alumni MIS dan terus memperoleh update informasi terkait program di Merintis Indonesia.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section-margin generic-margin padding-top-content" id="terms">
      <div class="container">
        <div class="section-intro text-lg-left pb-90px">
          <h1 class="hijau text-lg-left size-70px">SYARAT & KETENTUAN</h1>
          <h1 class="kuning text-lg-left">PROPOSAL BISNIS</h1>
          <ul class="tanda text-lg-left hitam px-3">
            <li class="py-1">Peserta melakukan registrasi dan mengupload seluruh kelengkapan pendaftaran melalui <b>www.merintisindonesia.com</b> paling lambat tanggal <b>31 Januari 2021</b></li>
            <li class="py-1">Format dokumennya, dalam bentuk ‘template’, telah disediakan panitia, dan dapat diakses melalui <a href="https://bit.ly/ProposalBisnisMIS2021" target="_blank"><b>bit.ly/ProposalBisnisMIS2021</b></a></li>
            <li class="py-1">Setiap peserta hanya boleh mengikutsertakan satu (1) Proposal Bisnis.</li>
            <li class="py-1">Proposal Bisnis menjadi milik panitia, namun Hak Cipta dan ide bisnis tetap milik peserta.</li>
          </ul>
           <a class="button button-hero mt-4" href="#registration">Daftar Sekarang</a>
        </div>
      </div>
      <div class="container">
        <div class="section-intro text-lg-left pb-90px">
          <h1 class="hijau text-lg-left size-70px">SYARAT & KETENTUAN</h1>
          <h1 class="kuning text-lg-left">IDE BISNIS</h1>
          <ul class="tanda text-lg-left hitam px-3">
            <li class="py-1">Bukan merupakan bisnis/usaha yang dilarang atau bertentangan dengan hukum negara Republik Indonesia.</li>
            <li class="py-1">Ide bisnis yang inovatif dan solutif untuk diimplementasikan dan menjawab permasalahan yang ada di daerah sekitar.</li>
            <li class="py-1">Bisa merupakan bisnis dalam bidang Food & Beverage, Industri Kreatif/Kerajinan, Teknologi Informasi dan Komputer, Industri Proses/Manufaktur, Edukasi, Pariwisata,  Kesehatan, Kecantikan, dan Perdagangan.</li>
            <li class="py-1">Diwakilkan kepada satu (1) peserta, apabila ide bisnis dimiliki oleh lebih dari satu (1) orang.</li>
            <li class="py-1">Produk/layanan ide bisnis tidak harus dalam bentuk website/app, melainkan menggunakan media teknologi dalam salah satu proses operasional/produksi/marketing/dan sebagainya.</li>
            <li class="py-1">Harus orisinil, milik atau ciptaan sendiri, dan bukan merupakan proyek atau bagian dari proses studi / perkuliahan.</li>
          </ul>
        </div>
      </div>
      <div class="container">
        <div class="section-intro text-lg-left pb-90px">
          <h1 class="hijau text-lg-left size-70px">SYARAT & KETENTUAN</h1>
          <h1 class="kuning text-lg-left">KEPESERTAAN</h1>
          <!-- SK KEPESERTAAN -->
          <ul class="tanda text-lg-left hitam px-3">
            <li class="py-1">Warga Negara Indonesia.</li>
            <li class="py-1">Dibuka untuk pelajar dan umum.</li>
            <li class="py-1">Berusia minimal 15 tahun hingga maksimal 35 tahun per tahun 2021.</li>
            <li class="py-1">Sehat jasmani dan rohani, serta terbebas dari narkoba.</li>
            <li class="py-1">Berkeinginan untuk berwirausaha atau sudah menjadi wirausaha pemula.</li>
            <li class="py-1">Bersedia untuk mengikuti seluruh rangkaian timeline apabila terpilih menjadi 30 finalis untuk melakukan mentoring dan final pitching.</li>
          </ul>
          <div class="section-intro text-center">
            <a class="button button-hero mt-4" href="#registration">Daftar Sekarang</a>
            <!-- <a class="button button-hero mt-4" href="#pengumuman">Cek Hasil TOP 50</a> -->
          </div>
        </div>
      </div>
    </section>

    <section class="section-margin generic-margin padding-top-content" id="kriteria">
      <div class="container">
        <div class="section-intro text-lg-left pb-90px">
          <h1 class="hijau text-lg-left size-70px">KRITERIA SELEKSI</h1>
          <h1 class="kuning text-lg-left">MERINTIS INDONESIA SUMMIT 2021</h1>
          <div class="row">
            <div class="col-md-6 pd-top">
              <img src="<?= base_url('assets/img/Picture5.png'); ?>" class="img-smalll float-left" alt="kriteria-seleksi-1">
              <h4 class="hijau size-20px">Ide Bisnis Inovatif dan Kreatif</h4>
              <p class="kuning">Ide bisnis  harus memiliki keunggulan dalam pemecahan masalah</p>
            </div>
            <div class="col-md-6 pd-top">
              <img src="<?= base_url('assets/img/Picture10.png'); ?>" class="img-smalll float-left" alt="kriteria-seleksi-2">
              <h4 class="hijau size-20px">Pemahaman Aspek Bisnis</h4>
              <p class="font-bold kuning">Kemampuan analisa pasar dan perencanaan, pemasaran</p>
            </div>
            <div class="col-md-6 pd-top">
              <img src="<?= base_url('assets/img/Picture6.png'); ?>" class="img-smalll float-left" alt="kriteria-seleksi-3">
              <h4 class="hijau size-20px">Potensi Pengembangan Bisnis</h4>
              <p class="font-bold kuning">Seberapa spesifik market size, target dan potensi pasar di wilayah sekitar</p>
            </div>
            <div class="col-md-6 pd-top">
              <img src="<?= base_url('assets/img/Picture9.png'); ?>" class="img-smalll float-left mt-4 mt-md-3" alt="kriteria-seleksi-4">
              <h4 class="hijau size-20px">Strategi Penjualan</h4>
              <p class="font-bold kuning">Tergabung dalam lingkup koneksi dan komunitas digital kreatif Mataraman</p>
            </div>
            <div class="col-md-6 pd-top">
              <img src="<?= base_url('assets/img/Picture7.png'); ?>" class="img-smalll float-left mt-4 mt-md-0" alt="kriteria-seleksi-5">
              <h4 class="hijau size-20px">Pemahaman Aspek Keuangan</h4>
              <p class="font-bold kuning">Dari data penyusunan cost budgeting ide bisnis yang realistis dengan kebutuhan</p>
            </div>
            <div class="col-md-6 pd-top">
              <img src="<?= base_url('assets/img/Picture8.png'); ?>" class="img-smalll float-left" alt="kriteria-seleksi-6">
              <h4 class="hijau size-20px">Impact Sosial </h4>
              <p class="font-bold kuning">Seberapa besar impact sosial yang berpengaruh di masyarakat sekitar</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section-margin generic-margin padding-top-content" id="seleksi">
      <div class="container">
        <div class="section-intro text-lg-left pb-100px">
          <h1 class="hijau text-lg-left size-70px">PROSES SELEKSI</h1>
          <h1 class="kuning text-lg-left">MERINTIS INDONESIA SUMMIT 2021</h1>
          <!--<img  style="max-width: 100%;" class="img-bigg" src="img/assets/Picture16.png" alt="">-->
          <img style="max-width: 100%;" class="mt-3" src="<?= base_url('/assets/img/Picture16.png'); ?>" alt="">
        </div>
      </div>
    </section>

    <!-- <section class="section-margin generic-margin padding-top-content" id="pengumuman">
      <div class="container">
        <div class="section-intro text-center pb-100px">
          <h1 class="kuning">PENGUMUMAN TOP 50</h1>
          <h1 class="hijau">MERINTIS INDONESIA SUMMIT 2021</h1> -->

          <!-- Kotak Cari -->
          <!-- <div class="form-inline my-3">
            <input class="form-control mr-sm-2" type="search" name="nama_tim" placeholder="Cari Nama Finalis" aria-label="Cari">
            <button class="btn btn-outline-success my-2 my-sm-0" onclick="cariData()">Cari</button>
          </div>

          <div id="tabel-finalis">
            <table>
              <thead>
                <tr>
                  <th>No</th>
                  <th>ID Finalis</th>
                  <th>Nama Finalis</th>
                  <th>Nama Bisnis</th>
                </tr>
              </thead>
              <tbody id="dtPengumuman"></tbody>
            </table>
          </div>
        </div>
        <div class="section-intro text-center">
          <a class="button button-hero mt-4" href="#videopitch">Submit Video Pitching</a>
        </div>
      </div>
    </section class="section-margin generic-margin padding-top-content" id="pengumuman"> -->

    <!-- <section class="section-margin generic-margin padding-top-content" id="videopitch">
      <div class="container">
        <div class="section-intro text-center pb-100px">
          <img class="img-100 mb-4" src="<?= base_url('assets/img/logo-mis-color.png'); ?>" alt="logo-mis">
          <h1 class="kuning">VIDEO PITCHING</h1>
          <h1 class="hijau">MERINTIS INDONESIA SUMMIT 2021</h1>
          <p class="video-pitch text-left mt-4 mb-2"><b>Link Youtube Video Pitching</b> <small>(Panduan: <a href="https://bit.ly/VideoPitchingMIS2021">bit.ly/VideoPitchingMIS2021</a>)</small></p> -->
          <!-- ALERT -->
          <!-- <div id="alertGagal"></div> -->
          <!-- END ALERT -->
          <!-- <form method="post" id="formVideo" class="custom-form">
            <div class="form-group">
              <input type="hidden" name="id_user" value="" id="id_user">
              <div class="lay-video-outline">
                <input type="text" class="form-control video" placeholder="(lampirkan link youtube di sini)" name="videopitch">
              </div>
              <div id="err-video" class="invalid-feedback">
                Pesan error link video
              </div>
            </div>
          </form>
          <p class="text-left"><i class="fas fa-exclamation-circle kuning"></i> Deadline pengumpulan: 10 Februari 2021 pukul 17.00 WIB</p>
          <button id="btnSubmitVideo" class="button button-hero border-0 mt-3" onclick="submitVideo()">Submit</button>
        </div>
      </div>
    </section> -->

    <section class="section-margin generic-margin padding-top-content" id="registration">
      <div class="container search-wrapper px-sm-2">
        <form method="post" id="formProposal" class="search-form" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-12">
              <!--============ ALERT PROPOSAL -->
              <div id="alert-proposal">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Pendaftaran Telah Ditutup
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                </div>
              </div>
              <!--============ END ALERT PROPOSAL -->
            </div>
            <input type="hidden" name="id_user" value="<?= $_SESSION['id_akun']; ?>" id="id_user">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Nama Lengkap" name="nm_lengkap">
                <div class="invalid-feedback">
                  Pesan error nama
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Email" name="email">
                <div class="invalid-feedback">
                  Pesan error email
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="No. Handphone" name="no_hp">
                <div class="invalid-feedback">
                  Pesan error nomor hp
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir">
                <div class="invalid-feedback">
                  Pesan error tempat lahir
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><small>Tanggal Lahir</small></span>
                  </div>
                  <input type="date" class="form-control" placeholder="Tanggal lahir" name="tgl_lahir">
                  <div class="invalid-feedback">
                    Pesan error tanggal lahir
                  </div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Kota Domisili" name="kota_domisili">
                <div class="invalid-feedback">
                  Pesan error kota domisili
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Pekerjaan" name="pekerjaan">
                <div class="invalid-feedback">
                  Pesan error pekerjaan
                </div>
              </div>
              <div class="form-group">
                <select name="jenis_kelamin" id="gender">
                  <option value="" disabled selected>Jenis Kelamin</option>
                  <option value="L">Laki - Laki</option>
                  <option value="P">Perempuan</option>
                </select>
                <div class="small-red"></div>
              </div>
              <div class="form-group">
                <select name="role" id="role">
                  <option value="" disabled selected>Role</option>
                  <option value="hacker">Hacker (IT)</option>
                  <option value="hipster">Hipster (Design)</option>
                  <option value="hustler">Hustler (Marketing)</option>
                </select>
                <div class="small-red"></div>
              </div>
            </div>

            <div class="col-md-6 col-sm-12">
              <div class="form-group text-left">
                <span class="font-weight-light">Template proposal: <a href="https://bit.ly/ProposalBisnisMIS2021" target="_blank">https://bit.ly/ProposalBisnisMIS2021</a></span>
              </div>
              <div class="form-group text-left mt-3">
                <small style="padding-left: 5px; padding-bottom: 5px;">Upload File Proposal Bisnis dalam bentuk pdf</small>
                <input type="file" class="form-control" id="file-proposal" name="url_proposal">
                <div class="invalid-feedback">
                  Pesan error Proposal
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Bidang Ide Bisnis (Ex. F&B, Fashion,Agrikultur, Pariwisata, dll.)" name="bidang_bisnis">
                <div class="invalid-feedback">
                  Pesan error Bidang Bisnis
                </div>
              </div>
              <div class="form-group">
                <select name="metode_bayar" id="pay_method">
                  <option value="" disabled selected>Metode Pembayaran</option>
                  <option value="bank_transfer">Bank Transfer</option>
                  <option value="gopay">Gopay</option>
                  <option value="dana">Dana</option>
                </select>
                <div class="small-red"></div>
              </div>
              <div class="form-group text-left">
                <small>
                  Keterangan Metode Pembayaran:<br/>
                  HTM: Rp50.000,-
                  <ol style="margin-left: -20px">
                    <li>Bank Transfer CIMB <br/>
                      705656836300 (a/n Roro Mega Cahyaning 'Azmi Riyandani)
                    </li>
                    <li>Gopay/Dana <br/>
                      085785036770 (a/n Roro Mega Cahyaning 'Azmi Riyandani)
                    </li>
                  </ol>
                </small>
              </div>
              <div class="form-group" style="text-align: left;">
                <small style="padding-left: 5px; padding-bottom: 5px;">Upload Bukti Pembayaran</small>
                <input type="file" class="form-control" id="file-bBayar" name="url_bukti_bayar">
                <div class="invalid-feedback">
                  Pesan error bukti bayar
                </div>
              </div>
              <div class="progress d-none" id="progress-upload">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
              </div>
              <div class="form-group">
                <button class="button border-0 mt-3" id="subProposal">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>

    <section class="section-margin generic-margin padding-top-content" id="contact">
      <div class="container">
        <div class="section-intro text-center pb-100px">
          <img style="max-width: 100%;" class="" src="<?= base_url('assets/img/Picture18.png'); ?>" alt="">

          <!--<h1 style="line-height: 100px" class="kuning size-120px text-lg-left h10">CONTACT US </h1>-->
          <h1 style="line-height: 100px" class="mt-4 kuning">CONTACT US</h1>
          <img style="max-width: 85%; margin-top: -15px;" class=" h11 fluid" src="<?= base_url('assets/img/Picture19.png'); ?>" alt="">
        </div>
      </div>
    </section>

    <footer class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 footer-contact" data-aos="fade-up">
            <img src=<?= base_url('assets/img/Picture21.png')?> alt="" class="img-fluid" style="">
            <br></br>
            <p>
              <b>Merintis Indonesia</b> <br>
              Jl. Jawa No.23, Kartoharjo <br>
              Kota Madiun, Jawa Timur 63122 <br>
              Indonesia <br><br>
              <strong>Phone:</strong> +62 857-8503-6770<br>
              <strong>Email:</strong> info@merintisindonesia.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="single-footer-widget">
              <h6>Navigation Links</h6>
              <ul>
                <li><a href="#beranda">Beranda</a></li>
                <li><a href="#tentang">Tentang</a></li>
                <!--<li><a href="#benefit">Benefits</a></li>-->
                <li><a href="#terms">Syarat Ketentuan</a></li>
                <li><a href="#kriteria">Kriteria Seleksi</a></li>
                <li><a href="#seleksi">Timeline</a></li>
                <!-- <li><a href="#registration">Pendaftaran</a></li> -->
                <li><a href="#pengumuman">Pengumuman</a></li>
                <li><a href="#contact">Contact</a></li>
              </ul>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="300">
            <h6>Ikuti Kami</h6>
            <div class="social-links mt-3">
              <a target="_blank" href="https://twitter.com/MerintisID?s=08" class="twitter"><button  type="button" class="btn btn-tw"><i class="fab fa-twitter"></i></button></a>
              <a target="_blank" href="https://www.facebook.com/merintisindonesia/" class="facebook"><button  type="button" class="btn btn-fb"><i class="fab fa-facebook-f"></i></button></a>
              <a target="_blank" href="https://instagram.com/merintis.indonesia/" class="instagram"><button  type="button" class="btn btn-ins"><i class="fab fa-instagram"></i></button></a>
              <a target="_blank" href="https://www.linkedin.com/company/merintis-indonesia" class="linkedin"><button  type="button" class="btn btn-li"><i class="fab fa-linkedin-in"></i></button></a>
            </div>
          </div>

          <div class="col-lg-1 col-md-10 footer-links" data-aos="fade-up" data-aos-delay="300">
            <div class="form-group">
              <a class="button border-0 mt-3" href="daftar">Bergabung</a>
            </div>
          </div>
        </div>

        <div class="footer-bottom row align-items-center">
          <p class="col-lg-16 col-sm-12 footer-text m-0 text-center"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>

        </div>
      </footer>
  <!--================Hero Banner Area End =================-->

  <script src="<?= base_url ('assets/vendors/jquery/jquery-3.2.1.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/vendors/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/vendors/owl-carousel/owl.carousel.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/vendors/nice-select/jquery.nice-select.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/js/jquery.ajaxchimp.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/js/main.js'); ?>"></script>
  <!-- SCRIPT DI BAWAH INI UNTUK VALIDASI FORM -->
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15/dist/smooth-scroll.polyfills.min.js"></script>
  <!-- SCRIPT DI ATAS INI UNTUK VALIDASI FORM -->
  <script>
  // UNTUK SMOOTH-SCROLL
  $(document).ready(function(){
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){

          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
  });

  // // ----------- CEK JIKA ADA SESSION
  // if (sessionStorage.getItem("session_id") === null && sessionStorage.getItem("token") === null) {
  //   // ---JIKA TIDAK ADA SESSION
  //   $(document).ready(function() {
  //     alertNoLogin();
  //     // REDIRECT KE HOME
  //     window.location.replace('<?= base_url(); ?>');
  //   })
  // } else {
  //   // ---JIKA ADA SESSION
  //   $("#alert-proposal").html();
  //   // ----------------- AMBIL DATA
  //   $(document).ready(function() {
  //     $.ajax({
  //       type: "post",
  //       url: "<?= base_url('/akun/ambillogin'); ?>",
  //       data: {"session_id": sessionStorage.getItem("session_id")},
  //       success: function(data) {
  //         // console.log(data);
  //         $("#id_user").val(data.id);
  //         $("input[name='nama_tim']").val(data.nm_lengkap);
  //         // $("input[name*='nm_lengkap']").val(data.nm_lengkap);
  //         // $("input[name*='email']").val(data.email);
  //       },
  //       error: function(response) {
  //         //console.log(response.responseJSON.message);
  //         alFixedGagal(response.responseJSON.message)
  //       }
  //     })
  //   })
  //   // ----------------- END AMBIL DATA
  // }
  // // ----------- END CEK JIKA ADA SESSION

  // ----------- CEK JIKA ADA SESSION
  let cekData = "<?= isset($_SESSION['token']) ? $_SESSION['token'] : ''; ?>";
  if (!cekData) {
    // ---JIKA TIDAK ADA SESSION
    $(document).ready(function() {
      alertNoLogin();
      // REDIRECT KE HOME
      window.location.replace('<?= base_url(); ?>');
    })
  } else {
    // ---JIKA ADA SESSION
    $("#alert-proposal").html();
    // ----------------- AMBIL DATA
    $(document).ready(function() {
      $.ajax({
        type: "post",
        url: "<?= base_url('/akun/ambillogin'); ?>",
        data: {"session_id": "<?= $_SESSION['id_akun']; ?>"},
        success: function(data) {
          // console.log(data);
          // $("#id_user").val(data.id);
          // $("input[name='nama_tim']").val(data.nm_lengkap);
          $("input[name*='nm_lengkap']").val(data.nm_lengkap);
          $("input[name*='email']").val(data.email);
        },
        error: function(response) {
          //console.log(response.responseJSON.message);
          alFixedGagal(response.responseJSON.message)
        }
      })
    })
    // ----------------- END AMBIL DATA
  }
  // ----------- END CEK JIKA ADA SESSION

  // ---------------- KUMPULAN ALERT PROPOSAL
  function alertNoLogin() {
    let alNoLogin = `<div class="alert alert-danger" role="alert">Silahkan Login Terlebih Dahulu!</div>`
    $("#alert-proposal").html(alNoLogin);
  }
  function alSuccessProp(pesan) {
    let alSuccess =
    `<div class="alert alert-success alert-dismissible fade show" role="alert">
    ${pesan}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>`
    $("#alert-proposal").html(alSuccess);
  }
  function alFailureProp(pesan) {
    let alFailure =
    `<div class="alert alert-warning alert-dismissible fade show" role="alert">
    ${pesan}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>`
    $("#alert-proposal").html(alFailure);
  }

  // ------------------ FIXED ALERT
  function alFixedBerhasil(pesan) {
    let alBerhasil = `
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    <div id="text-berhasil">${pesan}</div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    `
    $('.fixed-alert').html(alBerhasil)
    $('.fixed-alert').html(alBerhasil).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }

  function alFixedGagal(pesan) {
    let alGagal = `
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <div id="text-berhasil">${pesan}</div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    `
    $('.fixed-alert').html(alGagal).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }
  // -------------------------- END FIXED ALERT
  // ---------------- END KUMPULAN ALERT PROPOSAL

  // ---------------- TUTUP PENDAFTARAN
  $("#subProposal").attr('disabled', true);
  $("#subProposal").addClass('not-allowed');
  alFixedGagal("Pendaftaran Telah Ditutup");
  $(".form-control").eq(0).attr("disabled", true);
  $(".form-control").eq(1).attr("disabled", true);
  $(".form-control").eq(2).attr("disabled", true);
  $(".form-control").eq(3).attr("disabled", true);
  $(".form-control").eq(4).attr("disabled", true);
  $(".form-control").eq(5).attr("disabled", true);
  $(".form-control").eq(6).attr("disabled", true);
  $(".form-control").eq(7).attr("disabled", true);
  $(".form-control").eq(8).attr("disabled", true);
  $(".form-control").eq(9).attr("disabled", true);
  $("select[name=role]").attr("disabled",true);
  $("select[name=metode_bayar]").attr("disabled",true);
  $("select[name=jenis_kelamin]").attr("disabled",true);
  // ---------------- END TUTUP PENDAFTARAN

  // ----------- AMBIL DATA PENGUMUMAN 50
  // $(document).ready(function() {
  //   $.ajax({
  //     type: "get",
  //     url: "<?= base_url('/home/pengumuman'); ?>",
  //     success: function(data) {
  //       //console.log(data.length);
  //       //const bdTable = $("#dtPengumuman").innerHTML;
  //       let isiData = "";
  //       for(let i = 0; i < data.length; i++) {
  //         isiData +=
  //         `
  //         <tr>
  //           <td data-label="No">${i+1}</td>
  //           <td data-label="ID Finalis">${data[i].id_finalis}</td>
  //           <td data-label="Nama Finalis">${data[i].nm_lengkap}</td>
  //           <td data-label="Nama Bisnis">${data[i].nama_bisnis}</td>
  //         </tr>
  //         `;
  //       }
  //       $("#dtPengumuman").html(isiData);
  //     },
  //     error: function(err) {
  //       console.log(err);
  //     }
  //   });
  // })
  // ----------- END AMBIL DATA PENGUMUMAN 50

  // --------- CARI DATA PENGUMUMAN 50
  // function cariData() {
  //   let nmTim = $("input[name='nama_tim']").val();
  //   $.ajax({
  //     type: "post",
  //     url: "<?= base_url('/home/caridatafinalis'); ?>",
  //     data: {"nama_tim":nmTim},
  //     success: function(data) {
  //       //console.log(data);
  //       let isiData = "";
  //       if (data.length > 0) {
  //         for(let i = 0; i < data.length; i++) {
  //           isiData +=
  //           `
  //           <tr>
  //             <td data-label="No">${i+1}</td>
  //             <td data-label="ID Finalis">${data[i].id_finalis}</td>
  //             <td data-label="Nama Finalis">${data[i].nm_lengkap}</td>
  //             <td data-label="Nama Bisnis">${data[i].nama_bisnis}</td>
  //           </tr>
  //           `;
  //         }
  //         $("#dtPengumuman").html(isiData);
  //       } else {
  //         isiData +=
  //         `
  //         <tr><td colspan='4'>Data Tidak Ditemukan</td></tr>
  //         `;
  //         $("#dtPengumuman").html(isiData);
  //       }
  //     },
  //     error: function(err) {
  //       console.log(err);
  //     }
  //   });
  // }
  // --------- END CARI DATA PENGUMUMAN 50

  // ----------- SUBMIT VIDEO PITCH ------------
  // function submitVideo() {
  //   $.ajax({
  //     type: "post",
  //     url: "<?= base_url('/home/uploadlink'); ?>",
  //     data: $("#formVideo").serialize(),
  //     beforeSend: function() {
  //       $("#btnSubmitVideo").attr('disabled', true);
  //       $("#btnSubmitVideo").addClass('not-allowed');
  //     },
  //     success: function(data) {
  //       console.log(data);
  //       if (typeof data.success !== "undefined" && data.success == "false") {
  //         $(".lay-video-outline").addClass("video-invalid");
  //         videoErr();
  //         $("#err-video").html(data.error.videopitch);
  //       } else if (typeof data.success !== "undefined" && data.success == "true") {
  //         alFixedBerhasil('Berhasil Disimpan');
  //         videoErr();
  //         $("#err-video").html('');
  //         $(".lay-video-outline").removeClass("video-invalid");
  //         $("input[name='videopitch']").val('');
  //       } else {
  //         videoErr();
  //         $("#err-video").html('');
  //         $(".lay-video-outline").removeClass("video-invalid");
  //       }
  //       $("#btnSubmitVideo").attr('disabled', false);
  //       $("#btnSubmitVideo").removeClass('not-allowed');
  //     },
  //     error: function(err) {
  //       console.log(err);
  //     }
  //   });
  // }
  // ----------- END SUBMIT VIDEO PITCH ------------

  // ------------ CEK LOLOS --------------------
  // $(document).ready(function() {
  //   $.ajax({
  //     type: "post",
  //     url: "<?= base_url('/home/cekfinalismis'); ?>",
  //     data: {"id_akun" : "<?= $_SESSION['id_akun']; ?>"},
  //     success: function(data) {
  //       console.log(data);
  //       if (data.success == "true") {
  //         // Lolos TOP 50
  //         $("input[name='videopitch']").attr('disabled', false);
  //         $("#btnSubmitVideo").attr('disabled', false);
  //         $("#btnSubmitVideo").removeClass('not-allowed');
  //       } else {
  //         // Gagal TOP 50
  //         $("input[name='videopitch']").attr('disabled', true);
  //         $("#btnSubmitVideo").attr('disabled', true);
  //         $("#btnSubmitVideo").addClass('not-allowed');
  //         //alert gagal
  //         let alFailure =
  //         `<div class="alert alert-warning alert-dismissible fade show" role="alert">
  //         ${data.error}
  //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  //         <span aria-hidden="true">&times;</span>
  //         </button>
  //         </div>`
  //         $("#alertGagal").html(alFailure);
  //       }
  //     },
  //     error: function(err) {
  //       console.log(err);
  //     }
  //   });
  // })
  // ------------ END CEK LOLOS --------------------

  // ----- CEK INPUT LINK VIDEO ----------
  // function videoErr() {
  //   if($(".lay-video-outline").hasClass("video-invalid"))
  //   {
  //     $("#err-video").show();
  //   } else {
  //     $("#err-video").hide();
  //   }
  // }
  // ----- END CEK INPUT LINK VIDEO ----------

  // ------------------ KIRIM DATA PROPOSAL -------
  // $("#subProposal").on("click", function() {
  //   if (!cekData) {
  //     alFixedGagal("Pendaftaran Telah Ditutup");
  //   } else {
  //     $.ajax({
  //       xhr: function() {
  //         var xhr = new window.XMLHttpRequest();
  //         xhr.upload.addEventListener("progress", function(evt) {
  //           if (evt.lengthComputable) {
  //             var percentComplete = (evt.loaded / evt.total) * 100;
  //             //Do something with upload progress here
  //             $("#progress-upload .progress-bar").animate({
  //               width: percentComplete + '%'
  //             }, {
  //               duration: 1000
  //             });
  //             // Animate Text Loading
  //             $({ Counter: 0 }).animate({
  //               Counter: percentComplete
  //             }, {
  //               duration: 1400,
  //               easing: 'swing',
  //               step: function() {
  //                 $('#progress-upload .progress-bar').text(Math.ceil(this.Counter)+"%");
  //               }
  //             });
  //             // Batas animate Text Loading
  //           }
  //         }, false);
  //         return xhr;
  //       },
  //       type: "post",
  //       url: "<?= base_url('/akun/uploadproposal'); ?>",
  //       contentType: false,
  //       cache: false,
  //       processData: false,
  //       data: new FormData($('#formProposal')[0]),
  //       beforeSend: function() {
  //         $("#subProposal").attr('disabled', true);
  //         $("#subProposal").addClass('not-allowed');
  //         $("#progress-upload").removeClass("d-none");
  //       },
  //       success: function(data) {
  //         // console.log(data);
  //
  //         if (data.success === true) {
  //           // Progress-Bar and Button
  //           $("#subProposal").attr('disabled', false);
  //           $("#subProposal").removeClass('not-allowed');
  //           $("#progress-upload").addClass("d-none");
  //           $("#progress-upload .progress-bar").width("0%");
  //           // Alert
  //           alSuccessProp(data.message);
  //           alFixedBerhasil(data.message+"<br>Silahkan Cek E-mail");
  //           // CLEAR TEXT ERROR
  //           $(".form-control").eq(9).removeClass("is-invalid");
  //           $(".small-red").eq(1).html('');
  //           $(".form-control").eq(7).removeClass("is-invalid");
  //           $(".form-control").eq(8).removeClass("is-invalid");
  //           $(".small-red").eq(2).html('');
  //           $(".form-control").eq(4).removeClass("is-invalid");
  //           $(".form-control").eq(5).removeClass("is-invalid");
  //           $(".form-control").eq(6).removeClass("is-invalid");
  //           $(".small-red").eq(0).html('');
  //           $(".form-control").eq(0).removeClass("is-invalid");
  //           $(".form-control").eq(1).removeClass("is-invalid");
  //           $(".form-control").eq(2).removeClass("is-invalid");
  //           $(".form-control").eq(3).removeClass("is-invalid");
  //           // ---- BERSIHKAN FORM PROPOSAL
  //           $( '#formProposal' ).each(function(){
  //             this.reset();
  //           });
  //         } else {
  //           //alert GAGAL
  //           alFixedGagal("Pendaftaran Telah Ditutup")
  //           // Progress-Bar and Button
  //           $("#subProposal").attr('disabled', false);
  //           $("#subProposal").removeClass('not-allowed');
  //           $("#progress-upload").addClass("d-none");
  //           $("#progress-upload .progress-bar").width("0%");
  //           // ----- JIKA ADA PESAN
  //           if (data.message) {
  //             alFailureProp(data.message)
  //             alFixedGagal(data.message)
  //           }
  //           // ---- VALIDASI NAMA LENGKAP
  //           if (data.error.nm_lengkap) {
  //             $(".form-control").eq(0).addClass("is-invalid");
  //             $(".invalid-feedback").eq(0).html(data.error.nm_lengkap);
  //           } else {
  //             $(".form-control").eq(0).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI EMAIL
  //           if (data.error.email) {
  //             $(".form-control").eq(1).addClass("is-invalid");
  //             $(".invalid-feedback").eq(1).html(data.error.email);
  //           } else {
  //             $(".form-control").eq(1).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI NOMOR HP
  //           if (data.error.no_hp) {
  //             $(".form-control").eq(2).addClass("is-invalid");
  //             $(".invalid-feedback").eq(2).html(data.error.no_hp);
  //           } else {
  //             $(".form-control").eq(2).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI TEMPAT LAHIR
  //           if (data.error.tempat_lahir) {
  //             $(".form-control").eq(3).addClass("is-invalid");
  //             $(".invalid-feedback").eq(3).html(data.error.tempat_lahir);
  //           } else {
  //             $(".form-control").eq(3).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI TANGGAL LAHIR
  //           if (data.error.tgl_lahir) {
  //             $(".form-control").eq(4).addClass("is-invalid");
  //             $(".invalid-feedback").eq(4).html(data.error.tgl_lahir);
  //           } else {
  //             $(".form-control").eq(4).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI KOTA DOMISILI
  //           if (data.error.kota_domisili) {
  //             $(".form-control").eq(5).addClass("is-invalid");
  //             $(".invalid-feedback").eq(5).html(data.error.kota_domisili);
  //           } else {
  //             $(".form-control").eq(5).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI PEKERJAAN
  //           if (data.error.pekerjaan) {
  //             $(".form-control").eq(6).addClass("is-invalid");
  //             $(".invalid-feedback").eq(6).html(data.error.pekerjaan);
  //           } else {
  //             $(".form-control").eq(6).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI JENIS KELAMIN
  //           if (data.error.jenis_kelamin) {
  //             $(".small-red").eq(0).html(data.error.jenis_kelamin);
  //           } else {
  //             $(".small-red").eq(0).html('');
  //           }
  //           // ----- VALIDASI ROLE
  //           if (data.error.role) {
  //             $(".small-red").eq(1).html(data.error.role);
  //           } else {
  //             $(".small-red").eq(1).html('');
  //           }
  //           // ----- VALIDASI PROPOSAL
  //           if (data.error.url_proposal) {
  //             $(".form-control").eq(7).addClass("is-invalid");
  //             $(".invalid-feedback").eq(7).html(data.error.url_proposal);
  //           } else {
  //             $(".form-control").eq(7).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI BIDANG BISNIS
  //           if (data.error.bidang_bisnis) {
  //             $(".form-control").eq(8).addClass("is-invalid");
  //             $(".invalid-feedback").eq(8).html(data.error.bidang_bisnis);
  //           } else {
  //             $(".form-control").eq(8).removeClass("is-invalid");
  //           }
  //           // ----- VALIDASI METODE BAYAR
  //           if (data.error.metode_bayar) {
  //             $(".small-red").eq(2).html(data.error.metode_bayar);
  //           } else {
  //             $(".small-red").eq(2).html('');
  //           }
  //           // ----- VALIDASI BUKTI BAYAR
  //           if (data.error.url_bukti_bayar) {
  //             $(".form-control").eq(9).addClass("is-invalid");
  //             $(".invalid-feedback").eq(9).html(data.error.url_bukti_bayar);
  //           } else {
  //             $(".form-control").eq(9).removeClass("is-invalid");
  //           }
  //         }
  //       },
  //       error: function(response) {
  //         //console.log(response.responseJSON.message);
  //         alFixedGagal(response.responseJSON.message);
  //         // Progress-Bar and Button
  //         $("#subProposal").attr('disabled', false);
  //         $("#subProposal").removeClass('not-allowed');
  //         $("#progress-upload").addClass("d-none");
  //         $("#progress-upload .progress-bar").width("0%");
  //       }
  //     })
  //   }
  //   return false;
  // })
  // -------------------- END KIRIM DATA PROPOSAL

  // -------------------------- CEK SIZE FILE
  cekSizeProposal()
  cekSizebBayar()
  function cekSizeProposal() {
    let uploadField = document.getElementById("file-proposal");
    uploadField.onchange = function() {
      if(this.files[0].size > 10000024){
        $(".form-control").eq(7).addClass("is-invalid");
        $(".invalid-feedback").eq(7).html("Ukuran file maksimal 10MB");
      } else {
        $(".form-control").eq(7).removeClass("is-invalid");
      }
    }
  }

  function cekSizebBayar() {
    let uploadField = document.getElementById("file-bBayar")
    uploadField.onchange = function() {
      if(this.files[0].size > 5000024){
        $(".form-control").eq(9).addClass("is-invalid");
        $(".invalid-feedback").eq(9).html("Ukuran file maksimal 5MB");
      } else {
        $(".form-control").eq(9).removeClass("is-invalid");
      }
    }
  }
  // -------------------------- END CEK SIZE FILE

  // -------- SCROLL SPY
  let beranda = document.querySelector("#beranda");
  let tentang = document.querySelector("#tentang");
  let terms = document.querySelector("#terms");
  let kriteria = document.querySelector("#kriteria");
  let seleksi = document.querySelector("#seleksi");
  let registrasi = document.querySelector("#registration");
  // let pengumuman = document.querySelector("#pengumuman");
  let contact = document.querySelector("#contact");

  window.addEventListener("scroll", () => {
    var windo = window.pageYOffset+70;
    if (beranda.offsetTop <= windo && tentang.offsetTop > windo) {
      $(".nav-item").eq(1).removeClass("active");
      $(".nav-item").eq(0).addClass("active");
      // console.log("beranda");
    }
    else if (tentang.offsetTop <= windo && terms.offsetTop > windo) {
      $(".nav-item").eq(0).removeClass("active");
      $(".nav-item").eq(2).removeClass("active");
      $(".nav-item").eq(1).addClass("active");
      // console.log("tentang");
    }
    else if (terms.offsetTop <= windo && kriteria.offsetTop > windo) {
      $(".nav-item").eq(1).removeClass("active");
      $(".nav-item").eq(3).removeClass("active");
      $(".nav-item").eq(2).addClass("active");
      // console.log("terms");
    }
    else if (kriteria.offsetTop <= windo && seleksi.offsetTop > windo) {
      $(".nav-item").eq(2).removeClass("active");
      $(".nav-item").eq(4).removeClass("active");
      $(".nav-item").eq(3).addClass("active");
      // console.log("kriteria");
    }
    else if (seleksi.offsetTop <= windo && registrasi.offsetTop > windo) {
      $(".nav-item").eq(3).removeClass("active");
      $(".nav-item").eq(5).removeClass("active");
      $(".nav-item").eq(4).addClass("active");
      // console.log("seleksi");
    }
    // else if (seleksi.offsetTop <= windo && pengumuman.offsetTop > windo) {
    //   $(".nav-item").eq(3).removeClass("active");
    //   $(".nav-item").eq(5).removeClass("active");
    //   $(".nav-item").eq(4).addClass("active");
    //   // console.log("seleksi");
    // }
    else if (registrasi.offsetTop <= windo && contact.offsetTop > windo) {
      $(".nav-item").eq(4).removeClass("active");
      $(".nav-item").eq(6).removeClass("active");
      $(".nav-item").eq(5).addClass("active");
      // console.log("registrasi");
    }
    // else if (pengumuman.offsetTop <= windo && contact.offsetTop > windo) {
    //   $(".nav-item").eq(4).removeClass("active");
    //   $(".nav-item").eq(6).removeClass("active");
    //   $(".nav-item").eq(5).addClass("active");
    //   // console.log(pengumuman.offsetTop);
    // }
    else if (contact.offsetTop <= windo) {
      $(".nav-item").eq(5).removeClass("active");
      $(".nav-item").eq(6).addClass("active");
      // console.log("contact");
    }
  })
  // -------- SCROLL SPY

  </script>
</body>
</html>
